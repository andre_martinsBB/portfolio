## -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import train_test_split
from sklearn.neighbors import KernelDensity
from sklearn.metrics import accuracy_score



def load_data(filename):
   return np.loadtxt(filename,delimiter=",")

def randomizer(data):
    ranks = np.arange(data.shape[0])
    np.random.shuffle(ranks)
    data = data[ranks,:]
    return data

def standardize(x):
    means = np.mean(x, axis=0)
    devs = np.std(x, axis=0)
    x = (x-means)/devs
    return x

def calc_fold(x,y, train_ix, test_ix, C):
    reg = LogisticRegression(penalty='l2',C=C, tol=1e-10)
    reg.fit(x[train_ix,:],y[train_ix])
    prob = reg.predict_proba(x[:,:])[:,1]    
    squares = (prob-y)**2
    return np.mean(squares[train_ix]),np.mean(squares[test_ix])

def calc_best (x,y,x_t, y_t,C):
    reg = LogisticRegression(penalty='l2',C=C, tol=1e-10)
    reg.fit(x,y)
    train = 1 - reg.score(x , y)
    va  =  1 - reg.score(x_t, y_t )
    matrizPredictL = reg.predict(x_t)
    return (train,va,matrizPredictL)

def calc_fold_KNN(x,y,train_ix, text_ix, k):
    reg = KNeighborsClassifier(n_neighbors=k)
    reg.fit(x[train_ix,:],y[train_ix])
    train = 1- reg.score(x[train_ix,:] , y[train_ix])
    va  =  1 - reg.score(x[test_ix,:], y[test_ix] )
    return (train,va)

def calc_KNN_best (x,y,x_t,y_t, k):
    reg = KNeighborsClassifier(n_neighbors=k)
    reg.fit(x,y)
    train = 1- reg.score(x, y)
    va  =  1 - reg.score(x_t, y_t)
    matrizPredictKNN = reg.predict(x_t)
    return (train,va,matrizPredictKNN)

   
def calc_McNemar (matrizPredictMod1,matrizPredictMod2):
    e01=0
    e10=0     
    for li in range(0,Y_t.shape[0]):    
        if(matrizPredictMod1[li]!=Y_t[li]):
            if(matrizPredictMod2[li]==Y_t[li]):
                e01=e01+1
        if(matrizPredictMod2[li]!=Y_t[li]):
            if(matrizPredictMod1[li]==Y_t[li]):
                e10=e10+1
    
    mcNemarProb=((abs(e01-e10)-1)**2)/(e01+e10)
       
    return mcNemarProb

    

data = load_data("TP1-data.csv")
data = randomizer(data)
y = data[:,4]
x = data[:,:-1]
x = standardize(x)


#divide no conjunto de treino (X_r,Y_r) e no de teste (X_t, Y_t)
X_r,X_t,Y_r,Y_t = train_test_split(x, y, test_size=0.33,stratify = y)


folds = 5
kf =StratifiedKFold(Y_r,n_folds = folds)

#LOGISTIC REGRETION
c=1
errors = []
for i in range(0,20):
    tr_err = valid_err = 0
    for train_ix, test_ix in kf:
        r,t = calc_fold(X_r,Y_r,train_ix,test_ix,c)
        tr_err += r
        valid_err += t
    errors.append( (c,tr_err/folds,valid_err/folds) )
    c=c*2


(best_c,value) = (1,1);
for (c,train_err, valid_err) in errors:
    if valid_err <value:
        (best_c,value)=(int(c),valid_err)


train , valid, matrizPredictL = calc_best(X_r,Y_r, X_t,Y_t,best_c)
print ( "Logistic Regretion best:  C=", best_c ,"  test error= ",valid )

errors =  np.array(errors) 
plt.figure(figsize = (9,6), frameon = False)
plt.plot(np.log2(errors[:,0]),errors[:,1], 'b', label="train")
plt.plot(np.log2(errors[:,0]),errors[:,2], 'r', label="validation")
plt.legend(loc=1,ncol=1, borderaxespad=0.1)
plt.ylabel('Error')
plt.xlabel('Log (C)')
plt.xlim(1,None)
plt.show()



#KNN
errors_KNN= []
for k in range(1,39,2):
    tr_err = valid_err = 0
    for train_ix, test_ix in kf:
        r,t = calc_fold_KNN(X_r,Y_r,train_ix,test_ix,k)
        tr_err += r
        valid_err += t
    errors_KNN.append( (k,tr_err/folds,valid_err/folds) )

(best_k,value) = (1,1);
for (k,train_err, valid_err) in errors_KNN:
    if valid_err <value:
        (best_k,value)=(k,valid_err)

train, valid,matrizPredictKNN = calc_KNN_best(X_r, Y_r, X_t, Y_t, best_k)
print ("KNN best: K=",best_k, "  test error=",valid)

errors_KNN = np.array(errors_KNN)
plt.figure(figsize = (9,6), frameon = False)
plt.plot(errors_KNN[:,0],errors_KNN[:,1], 'b', label="train")
plt.plot(errors_KNN[:,0],errors_KNN[:,2], 'r', label="validation")
plt.legend(loc=1,ncol=1, borderaxespad=0.1)
plt.xlim(1,None)
plt.ylabel('Error')
plt.xlabel('K Neighbors')
plt.show()




#NAIVE BAYES
def kernel_density(X_r,bw):
    kde = KernelDensity(kernel='gaussian', bandwidth=bw)
    kde.fit(X_r)
    return kde


def predict( X_test, Y_test,kdes):
    predicted = []
    sizeC0 = X_test[Y_test==0,:].shape[0]
    sizeC1 = X_test[Y_test==1,:].shape[0]
    total = sizeC0+sizeC1
    a0=[]
    a1=[]
    for k in range (0,4):
        a0.append( kdes[k,1].score_samples(X_test[:,[k]] )  )
                    
        a1.append( kdes[k,2].score_samples(X_test[:,[k]]))
        
    a0=np.array(a0)
    a1=np.array(a1)
    for i in range(0, total):
        naive0 =  np.log( sizeC0/total ) + (a0[0,i]+ a0[1,i]+a0[2,i]+a0[3,i]) 
        
        naive1 =  np.log( sizeC1/total ) + (a1[0,i]+ a1[1,i]+a1[2,i]+ a1[3,i])
        if naive0>naive1:
            predicted.append(0)
        else:
            predicted.append(1)
            
    return np.array(predicted)



def fit_naive(X_train, Y_train, bw):
    kdes = []
    X_real_train= X_train[Y_train==0,:] 
    X_fake_train= X_train[Y_train==1,:]
    for feature  in range(0,4):
        #kde classe 0 da "feature"
        kde = kernel_density(X_real_train[:,[feature]],bw)
                
        #kde classe 1 da "feature"
        kde2 = kernel_density(X_fake_train[:,[feature]],bw)
                
        kdes.append((feature,kde,kde2))
    return np.array(kdes)
    
    

best_naives = [];
bandwidth_values = np.arange(0.01,1.0,0.02)

scores = []
for bw in bandwidth_values:
        train_error = valid_error =0 
        for train_ix, test_ix in kf:
            
            X_train = X_r[train_ix,:]
            Y_train = Y_r[train_ix]
                
            kdes = fit_naive(X_train, Y_train,bw)
            
            # testes
            X_test = X_r[test_ix,:]
            Y_test = Y_r[test_ix]
            
            predicted_train = predict(X_train,Y_train,kdes)
        
            predicted_test = predict(X_test,Y_test,kdes)
                
            train_error += 1 - accuracy_score(Y_train, predicted_train)
            valid_error += 1 - accuracy_score(Y_test, predicted_test)
            
        scores.append( (bw, train_error/folds, valid_error/folds))

(best_w,t_error_w)=(0.01,1)
for (bw, train, test) in scores:
    if test < t_error_w:
        (best_w,t_error_w) = (bw,test)

kdes=fit_naive(X_t,Y_t,best_w)
matrizPredictNB = predict(X_t,Y_t,kdes)

print ("Naives best bandwith:", best_w, " test error:", 1-accuracy_score(Y_t,matrizPredictNB) )


scores = np.array(scores)
plt.figure(figsize = (9,6), frameon = False)
plt.plot(scores[:,0],scores[:,1], 'b',label="train")
plt.plot(scores[:,0],scores[:,2], 'r',label="validation")
plt.legend(loc=1,ncol=1, borderaxespad=0.1)
plt.xlim(0,None)
plt.ylabel('Error')
plt.xlabel('bandwidth')
plt.show()
plt.close()


print("Logistic VS KNN: ", calc_McNemar(matrizPredictL,matrizPredictKNN) )
print("KNN VS Naive Bayes: ", calc_McNemar(matrizPredictKNN,matrizPredictNB) )
print("Naive Bayes VS Logistic: ", calc_McNemar(matrizPredictNB,matrizPredictL) )




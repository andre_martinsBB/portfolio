import tensorflow as tf
import numpy as np
import keras as keras
from datetime import datetime



# ------------------------------ Load data and tensor variables initialization ------------------------------#

mnist  = keras.datasets.mnist
#Split train and test data
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

num_epochs = 250
num_features = 784
batch_size = 100
num_labels = 10

#TF placeholders
features = tf.placeholder(tf.float32, shape=(None, num_features))
labels = tf.placeholder(tf.int64, shape=(None))

# ------------------------------------------- Reshape-DATA -------------------------------------------#

valid_images = train_images[50000:].reshape(-1,28*28)
valid_labels = train_labels[50000:]
train_images = train_images[:50000].reshape(-1,28*28)
train_labels = train_labels[:50000]
test_images = test_images.reshape(-1,28*28)

#Enconding integer labels to vector of 0's and 1.
one_hot_labels = tf.one_hot(labels, depth = num_labels, on_value = 1.0, off_value = 0.0, axis = -1)
# ----------------------------------------------------------------------------------------------------#




# ------------------------------------ Multi-Layer Perceptron Model ----------------------------------#
# Cria as layers do mlp para um problema n-ario de classificação (last layer doenst have activation function, softmax)
def create_layer_MLP(inputs, neurons, name, lastLayer):
    with tf.name_scope(name):
        weights = tf.Variable(tf.random_normal((int(inputs.shape[1]), neurons)), name="weights")
        bias = tf.Variable(tf.zeros([neurons]), name="bias")
        net = tf.add(tf.matmul(inputs, weights), bias, name="sum_layer")
        if not lastLayer:
            output = tf.nn.sigmoid(net, name="activ_funct")
        else:
            output = net
    return output

with tf.name_scope("MLP"):
    layer1 = create_layer_MLP(features, 500, 'hidden_layer1', False)
    layer2 = create_layer_MLP(layer1, 300, 'hidden_layer2', False)
    layer3 = create_layer_MLP(layer2, 10, 'outputLayer', True)
    last_layer = layer3
# ----------------------------------------------------------------------------------------------------#

# -------------Create cost function(loss) and optimizer,define training condition(minimize)------------------------#
#cost function
cost = tf.reduce_mean(tf.losses.softmax_cross_entropy(onehot_labels=one_hot_labels, logits=last_layer))

with tf.name_scope("Evaluation_metrics/Accuracy"):
    class_probabilities = tf.nn.softmax(last_layer)
    prediction = tf.argmax(class_probabilities, axis=1)
    accuracy = tf.reduce_mean(tf.cast(tf.equal(labels, prediction), tf.float32))

cost_summary = tf.summary.scalar("Cost", cost)
accuracy_summary_train = tf.summary.scalar("Accuracy_train", accuracy)
accuracy_summary_validation = tf.summary.scalar("Accuracy_Validation", accuracy)
true_error_summary = tf.summary.scalar("Accuracy_True_Error", accuracy)


learning_rates = [0.1, 0.2, 0.4]
opt_functions = [[tf.train.GradientDescentOptimizer, "GradientDescent"], [tf.train.AdamOptimizer,"AdamOptimizer"], [tf.train.AdagradOptimizer, "AdagradOptimizer"]]

root_logdir = "logs"

for opt in opt_functions:
    for lr in learning_rates:
        logdir = "{}/MLP(500_300)-LR_{}-OPT_{}/".format(root_logdir, lr, opt[1])
        with tf.name_scope("Train"):
            optimizer = opt[0](learning_rate=lr)
            training_op = optimizer.minimize(loss=cost)

        shuffled = np.array(range(train_images.shape[0]))

        with tf.Session() as session:
            tf.global_variables_initializer().run()
            file_writer = tf.summary.FileWriter(logdir, session.graph)
            for epoch in range(num_epochs):
                offset = 0
                np.random.shuffle(shuffled)
                while offset<train_images.shape[0]:
                    batch_images = train_images[shuffled[offset:offset+batch_size]]
                    batch_labels = train_labels[shuffled[offset:offset+batch_size]]
                    batch_dict = {features : batch_images, labels : batch_labels}
                    session.run((training_op, cost), feed_dict=batch_dict)
                    offset += batch_size

                if epoch % 10 == 0 or epoch == 0:
                    print(epoch)
                    # file_writer.add_summary(cost_summary.eval(feed_dict=batch_dict), epoch)
                    file_writer.add_summary(cost_summary.eval({features: train_images, labels: train_labels}), epoch)
                    file_writer.add_summary(accuracy_summary_train.eval({features: train_images, labels: train_labels}), epoch)
                    file_writer.add_summary(accuracy_summary_validation.eval({features: valid_images, labels: valid_labels}), epoch)
            file_writer.add_summary(true_error_summary.eval({features: test_images, labels: test_labels}))
            file_writer.close()

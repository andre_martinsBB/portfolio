from tensorflow import keras
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, BatchNormalization, Conv2D, Dense, AveragePooling2D
from tensorflow.keras.layers import Activation, Flatten, Dropout

from keras.layers.advanced_activations import LeakyReLU, PReLU

# --------------------------------------------LOAD DATA-------------------------------------------------#
mnist  = keras.datasets.mnist
#Split train and test data
(temp_train_images, temp_train_labels), (test_images, test_labels) = mnist.load_data()

NUM_EPOCHS = 10
BATCH_SIZE = 100
INIT_LR = 0.1

# ------------------------- Pre-Processing Data (train,evaluation,test) --------------------------------#
#Reshape-DATA
train_images = temp_train_images[:50000].reshape((temp_train_images[:50000].shape[0], 28, 28, 1))
# Set numeric type to float32 from uint8 and Normalize value to [0, 1]
train_images = train_images.astype("float32") / 255.0
train_labels = temp_train_labels[:50000]
# Transform lables to one-hot encoding
train_labels = keras.utils.to_categorical(train_labels, 10)

valid_images = temp_train_images[50000:].reshape((temp_train_images[50000:].shape[0], 28, 28, 1))
valid_images = valid_images.astype("float32") / 255.0
valid_labels = temp_train_labels[50000:]
valid_labels = keras.utils.to_categorical(valid_labels, 10)

test_images = test_images.reshape((test_images.shape[0], 28, 28, 1))
test_images = test_images.astype("float32") / 255.0
test_labels = keras.utils.to_categorical(test_labels, 10)

# --------------------------------------------MODEL-------------------------------------------------#


def creatDiscriminativeModel(initialLR,bs, n_epochs):
    inputs = Input(shape=(28,28,1),name="inputs")
    layer = Conv2D(6, kernel_size=(5, 5), strides=(1, 1), padding="same", input_shape=(28,28,1))(inputs)
    layer = Activation("tanh")(layer)
    layer = BatchNormalization()(layer)
    layer = AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding="valid")(layer)

    layer = Conv2D(16, kernel_size = (5, 5), strides=(1, 1), padding="valid", input_shape=(28,28,1))(layer)
    layer = Activation("tanh")(layer)
    layer = AveragePooling2D(pool_size=(2, 2), strides=(2, 2), padding="valid")(layer)

    layer = Conv2D(120, kernel_size =(5, 5), strides=(1, 1), padding="valid", input_shape=(28,28,1))(layer)
    layer = Activation("tanh")(layer)
    layer = Flatten()(layer)

    layer = Dense(84)(layer)
    layer = Activation("tanh")(layer)
    #layer = Activation(LeakyReLU(0.2))(layer)
    layer = Dense(10)(layer)
    layer = Activation("softmax")(layer)

    model = Model(inputs, layer)
    #adamOpt = keras.optimizers.Adam(lr=initialLR, decay=initialLR / n_epochs)
    opt = SGD(lr=initialLR, momentum=0.9, decay=initialLR / n_epochs)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=["accuracy"])
    model.summary()

    model.fit(train_images, train_labels, validation_data=(valid_images, valid_labels), batch_size=BATCH_SIZE, epochs=NUM_EPOCHS)

    print("STARTING EVALUATING THE MODEL:")
    test_score = model.evaluate(test_images, test_labels)
    print(test_score)

creatDiscriminativeModel(INIT_LR,BATCH_SIZE,NUM_EPOCHS)
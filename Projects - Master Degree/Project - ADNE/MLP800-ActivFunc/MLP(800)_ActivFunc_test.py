import tensorflow as tf
import numpy as np
import keras as keras
from datetime import datetime



# ------------------------------ Load data and variables initialization ------------------------------#

mnist  = keras.datasets.mnist
#Split train and test data
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

num_epochs = 50
num_features = 784
batch_size = 100
num_labels = 10

#TF placeholders
features = tf.placeholder(tf.float32, shape=(None, num_features))
labels = tf.placeholder(tf.int64, shape=(None))
# ----------------------------------------------------------------------------------------------------#

# ------------------------------------------- Reshape-DATA -------------------------------------------#

valid_images = train_images[50000:].reshape(-1,28*28)
valid_labels = train_labels[50000:]
train_images = train_images[:50000].reshape(-1,28*28)
train_labels = train_labels[:50000]
test_images = test_images.reshape(-1,28*28)

#Enconding integer labels to vector of 0's and 1.
one_hot_labels = tf.one_hot(labels, depth = num_labels, on_value = 1.0, off_value = 0.0, axis = -1)


def create_layer_MLP(inputs, neurons, activationFunction, name, lastLayer):
    with tf.name_scope(name):
        weights = tf.Variable(tf.random_normal((int(inputs.shape[1]), neurons)), name="weights")
        bias = tf.Variable(tf.zeros([neurons]), name="bias")
        net = tf.add(tf.matmul(inputs, weights), bias, name="sum_layer")
        if not lastLayer:
            output = activationFunction(net, name="activ_funct")
        else:
            output = net
    return output

root_logdir = "logs"
act_functions = [[tf.nn.relu6, "ActivFunc_Relu6"], [tf.nn.leaky_relu,"ActivFunc_LeakyRelu"],[tf.nn.sigmoid,"ActivFunc_Sigmoid"]]

for actFunct in act_functions:

    logdir = "{}/MLP800-{}/".format(root_logdir, actFunct[1])

    with tf.name_scope("MLP"):
        layer1 = create_layer_MLP(features, 800, actFunct[0], 'hidden_layer', False)
        layer2 = create_layer_MLP(layer1, 10, actFunct[0], 'outputLayer', True)
        last_layer = layer2

    cost = tf.reduce_mean(tf.losses.softmax_cross_entropy(onehot_labels=one_hot_labels, logits=last_layer))

    with tf.name_scope("Train"):
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
        training_op = optimizer.minimize(loss=cost)

    with tf.name_scope("Evaluation_metrics/Accuracy"):
        class_probabilities = tf.nn.softmax(last_layer)
        prediction = tf.argmax(class_probabilities, axis=1)
        accuracy = tf.reduce_mean(tf.cast(tf.equal(labels, prediction), tf.float32))

    shuffled = np.array(range(train_images.shape[0]))

    cost_summary = tf.summary.scalar("Cost", cost)
    accuracy_summary_train = tf.summary.scalar("Accuracy_train", accuracy)
    accuracy_summary_validation = tf.summary.scalar("Accuracy_Validation", accuracy)
    true_error_summary = tf.summary.scalar("Accuracy_True_Error", accuracy)
    tf.summary.merge_all()

    with tf.Session() as session:
        tf.global_variables_initializer().run()
        file_writer = tf.summary.FileWriter(logdir, session.graph)
        for epoch in range(num_epochs):
            offset = 0
            np.random.shuffle(shuffled)
            while offset<train_images.shape[0]:
                batch_images = train_images[shuffled[offset:offset+batch_size]]
                batch_labels = train_labels[shuffled[offset:offset+batch_size]]
                batch_dict = {features : batch_images, labels : batch_labels}
                session.run((training_op, cost), feed_dict=batch_dict)
                offset += batch_size

            if epoch % 10 == 0 or epoch == 0:
                print(epoch)
                # file_writer.add_summary(cost_summary.eval(feed_dict=batch_dict), epoch)
                file_writer.add_summary(cost_summary.eval({features: train_images, labels: train_labels}), epoch)
                file_writer.add_summary(accuracy_summary_train.eval({features: train_images, labels: train_labels}), epoch)
                file_writer.add_summary(accuracy_summary_validation.eval({features: valid_images, labels: valid_labels}), epoch)

        file_writer.add_summary(true_error_summary.eval({features: test_images, labels: test_labels}))
        file_writer.close()

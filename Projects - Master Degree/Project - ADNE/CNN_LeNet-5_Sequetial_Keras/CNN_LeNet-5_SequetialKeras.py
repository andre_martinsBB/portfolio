from tensorflow.keras.optimizers import SGD
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import BatchNormalization,Conv2D,MaxPooling2D, AveragePooling2D
from tensorflow.keras.layers import Activation, Flatten, Dropout, Dense

import keras as keras


NUM_EPOCHS = 10
BATCH_SIZE = 100
INIT_LR = 0.1

# --------------------------------------------LOAD DATA-------------------------------------------------#
mnist  = keras.datasets.mnist
#Split train and test data
(temp_train_images, temp_train_labels), (test_images, test_labels) = mnist.load_data()

# ------------------------- Pre-Processing Data (train,evaluation,test) --------------------------------#
#Reshape-DATA
train_images = temp_train_images[:50000].reshape((temp_train_images[:50000].shape[0], 28, 28, 1))
# Set numeric type to float32 from uint8 and Normalize value to [0, 1]
train_images = train_images.astype("float32") / 255.0
train_labels = temp_train_labels[:50000]
# Transform lables to one-hot encoding
train_labels = keras.utils.to_categorical(train_labels, 10)

valid_images = temp_train_images[50000:].reshape((temp_train_images[50000:].shape[0], 28, 28, 1))
valid_images = valid_images.astype("float32") / 255.0
valid_labels = temp_train_labels[50000:]
valid_labels = keras.utils.to_categorical(valid_labels, 10)

test_images = test_images.reshape((test_images.shape[0], 28, 28, 1))
test_images = test_images.astype("float32") / 255.0
test_labels = keras.utils.to_categorical(test_labels, 10)

# --------------------------------------------MODEL-------------------------------------------------#
def creatModel():
    model = Sequential()
    #1-Convolutional Layer
    model.add(Conv2D(6, kernel_size=(5, 5), strides=(1, 1), activation="tanh", input_shape = (28, 28,1), padding ="same"))
    #2-Pooling Layer
    model.add(AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding="valid"))
    #3-Convolutional Layer
    model.add(Conv2D(16, kernel_size=(5, 5), strides=(1, 1), activation="tanh", padding ="valid"))
    #4-Pooling Layer
    model.add(AveragePooling2D(pool_size=(2, 2), strides=(2, 2), padding="valid"))
    #5- Convolutional Layer
    model.add(Conv2D(120, kernel_size=(5, 5), strides=(1, 1), activation="tanh", padding ="valid"))
    # Flatten the CNN output so that we can connect it with fully connected layers
    model.add(Flatten())
    #6 - Fully Connected Layer
    model.add(Dense(84, activation="tanh"))
    #7- Output Layer with softmax activation
    model.add(Dense(10, activation="softmax"))
    return model

opt = SGD(lr=INIT_LR, momentum=0.9, decay=INIT_LR/NUM_EPOCHS)
print("CREATING MODEL...")
model = creatModel()
model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])

print("MODEL SUMMARY:")
print(model.summary())

#Train  Model
print("STARTING TRAINING THE MODEL:")
history = model.fit(train_images, train_labels, validation_data=(valid_images, valid_labels), batch_size=BATCH_SIZE, epochs=NUM_EPOCHS)

#Evaluate Model
print("STARTING EVALUATING THE MODEL:")
test_score = model.evaluate(test_images, test_labels)
print("EVALUATION:")
print("Test loss {:.4f},Test accuracy {:.2f}%".format(test_score[0], test_score[1] * 100))

#Save training
#model.save_weights("fashion_model.h5")
#model_json = model.to_json()
#with open("fashion_model.json", "w") as json_file:
#    json_file.write(model_json)





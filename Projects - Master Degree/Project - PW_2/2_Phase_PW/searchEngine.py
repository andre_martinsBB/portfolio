from nltk.tokenize import TreebankWordTokenizer
# 1st Delivery imports
from sklearn.feature_extraction.text import CountVectorizer
import nltk
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input, decode_predictions
from keras.preprocessing import image
import json as json
from skimage.feature import hog
from skimage import exposure
from skimage import color
from skimage.color import rgb2gray
from skimage.io import imread
from skimage.transform import resize
from skimage import img_as_ubyte
from sklearn.preprocessing import normalize
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import pairwise_distances, classification_report

# 2nd Delivery imports
from random import shuffle
from keras.utils import to_categorical
from sklearn.preprocessing import MultiLabelBinarizer

import numpy
from sklearn.preprocessing import MultiLabelBinarizer
numpy.set_printoptions(threshold=numpy.nan)

# Hide all warnings
import warnings
warnings.filterwarnings('ignore')

# Global variables
imageRep = "images/"
imageQueryRep = "Query/query_images/"

distanceFunctions = ["cityblock", "cosine", "euclidean", "l1", "l2", "manhattan"]

with open('Query/edfest_2016_stories.json', encoding='utf-8') as data_file:
    jason_data = json.loads(data_file.read())

# Global features for time purposes


##################   Auxiliar methods   #####################

# IF text = True Return List_of_String
# IF text = false Return List_of_images
def getJson_Data(json, text):
    counter_Stories = 0
    stories = json['stories']
    imagesToAnalyze = []
    sentencesToAnalyze = []

    for story in stories:
        imagesFromSegments = []
        sentencesFromSegments = []
        # sentenceToAnalyze = json['event_name'] + ' / ' + story['story_title'] # APAGAR CASO SO QUEIRAMOS O TEXT DO SEGMENT
        sentence = ''
        segments = json['stories'][counter_Stories]['segments']
        for segment in segments:
            if (text == True):
                sentence = segment['text']
                sentencesFromSegments.append(sentence)
            else:
                imagesFromSegments.append(segment['image'])

        imagesToAnalyze.append(imagesFromSegments)
        sentencesToAnalyze.append(sentencesFromSegments)

        counter_Stories += 1
    # print(imagesToAnalyze)
    if (text == True):
        # print(sentencesToAnalyze)
        return sentencesToAnalyze
    else:
        # print(imagesToAnalyze)
        return imagesToAnalyze


def getCorpusTextFile(csv_nameFile):
    data = pd.read_csv(csv_nameFile, sep=';', encoding="utf-8")
    texts = data['text'].tolist()
    return texts


def getCorpusLabels(csv_nameFile):
    data = pd.read_csv(csv_nameFile, sep=';', encoding="utf-8")
    tempLabels = data['gt_class'].tolist()
    return tempLabels


def getImages(csv_nameFile):
    data = pd.read_csv(csv_nameFile, sep=';', encoding="utf-8")
    imagesUrl = data['image-url'].tolist()

    image_names = [i.split('/')[-1] for i in imagesUrl]
    # print(image_names)
    return image_names


def get_images_with_class(csv_nameFile):
    data = pd.read_csv(csv_nameFile, sep=';', encoding="utf-8")
    imagesUrl = data['image-url'].tolist()

    image_names = [i.split('/')[-1] for i in imagesUrl]
    # print(image_names)
    return image_names


def select_dist_func():

    while True:

        print("-> Available distance functions:")
        print("--> cb: CityBlock, c: Cosine, e: Euclidean, l1: l1, l2: l2, m: Manhattan")

        me = input(">>")

        if me == "cb":
            return distanceFunctions[0]
        elif me == "c":
            return distanceFunctions[1]
        elif me == "e":
            return distanceFunctions[2]
        elif me == "l1":
            return distanceFunctions[3]
        elif me == "l2":
            return distanceFunctions[4]
        elif me == "m":
            return distanceFunctions[5]


def select_lp_param():

    print("-> Param required: %label(default: 0.2), num_iterations(default: 10), alpha(default: 0.5), sigma(default: 1)")
    print("--->(hint: enter for all default)")
    params = input(">>")
    parse_params = params.split(" ")

    if len(parse_params) == 1:
        if parse_params[0] == "":
            return 0.2, 10, 0.5, 1
        else:
            return float(parse_params[0]), 10, 0.5, 1

    elif len(parse_params) == 2:
        return float(parse_params[0]), int(parse_params[1]), 0.5, 1
    elif len(parse_params) == 3:
        return float(parse_params[0]), int(parse_params[1]), float(parse_params[2]), 1
    else:
        return float(parse_params[0]), int(parse_params[1]), float(parse_params[2]), float(parse_params[3])


def select_lp_name_spaces_and_weights():
    print("-> Select name space if=0: disables, if>0: enables")
    print("-> Name space order: BOW, BOE, HOC, HOG")
    print("--->(max 2 name spaces)")
    print("---> Example: (1 0 1 0)")


    params = input(">>")
    parse_params = params.split(" ")
    return [float(parse_params[0]), float(parse_params[1]), float(parse_params[2]), float(parse_params[3])]


# ____________________________________________KNN_____________________________________________________#

def k_neighbours(q, X, metric="euclidean", k=10):
    # Check pairwise_distances function docs: http://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise_distances.html#sklearn.metrics.pairwise_distances
    dists = pairwise_distances(q, X, metric=metric)

    # Dists gets a shape 1 x NumDocs. Convert it to shape NumDocs (i.e. drop the first dimension)
    dists = np.squeeze(dists)
    sorted_indexes = np.argsort(dists)

    return sorted_indexes[:k], dists[sorted_indexes[:k]]


# ____________________________________________Printing_____________________________________________________#
# Receives the cropped image and computed histogram and prints them side by side
def printImgAndHOC(im, hist, bin_edges):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 6), sharex=False, sharey=False)
    ax1.axis('off')
    ax1.imshow(im)
    ax1.set_title('Input_Image', fontsize=20)

    ax2.bar(bin_edges[:-1], hist, width=1)
    ax2.set_xticks([])
    ax2.set_xlim(bin_edges[:-1].min() * -2, max(bin_edges.max(), hist.shape[0] * 1.3))
    ax2.set_title('Histogram of Colors', fontsize=20)
    plt.show()


# Receives the cropped image and the computed HOC but NOT resided
def printGrayImgAndHOG(img_gray, hog):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 6), sharex=True, sharey=True)

    ax1.axis('off')
    ax1.imshow(img_gray, cmap=plt.cm.gray)
    ax1.set_title('GrayScale_Image', fontsize=20)

    # Rescale histogram for better display
    hog_image_rescaled = exposure.rescale_intensity(hog)

    ax2.axis('off')
    ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
    ax2.set_title('Hog_Image', fontsize=20)
    plt.show()


######################################################################################################


# ____________________________________________IMAGE PROCESSING_____________________________________________________#

######### Utility #########

def center_crop_image(im):
    # print("Original shape: {}".format(im.shape))
    if im.shape[2] == 4:  # Remove the alpha channel
        im = im[:, :, 0:3]

    # Resize so smallest dim = 224, preserving aspect ratio
    h, w, _ = im.shape
    if h < w:
        im = resize(image=im, output_shape=(224, int(w * 224 / h)))
    else:
        im = resize(im, (int(h * 224 / w), 224))

    # Center crop to 224x224
    h, w, _ = im.shape
    im = im[h // 2 - 112:h // 2 + 112, w // 2 - 112:w // 2 + 112]
    # print("Final shape: {}".format(im.shape))
    return im


######### Histrogram of color processing #########

def getHistogramOfColor(im, bins=(4,4,4), hist_range=(256, 256, 256)):

    im_r = im[:,:,0]
    im_g = im[:,:,1]
    im_b = im[:,:,2]

    red_level = hist_range[0] / bins[0]
    green_level = hist_range[1] / bins[1]
    blue_level = hist_range[2] / bins[2]

    im_red_levels = np.floor(im_r / red_level)
    im_green_levels = np.floor(im_g / green_level)
    im_blue_levels = np.floor(im_b / blue_level)

    ind = im_blue_levels * bins[0] * bins[1] + im_green_levels * bins[0] + im_red_levels

    hist_r, bins_r = np.histogram(ind.flatten(), bins[0] * bins[1] * bins[2])

    return hist_r, bins_r


# Prepares the image for search with HOG
def prep_img_query_hoc(img_n, bins):
    img_q = imread(imageQueryRep + img_n)

    img_q_hsv = center_crop_image(img_q)
    img_q_hsv = color.rgb2hsv(img_q_hsv)
    img_int = img_as_ubyte(img_q_hsv)
    hist, bin_edges = getHistogramOfColor(img_int, bins=bins)

    image_q_feat = np.squeeze(normalize(hist.reshape(1, -1), norm="l2"))
    # print("Image query feature dimension: {}".format(image_q_feat.shape))

    return image_q_feat


# Extracts all HOG from "images" to be searched
def prep_img_feats_hoc(im_db, bins=(4, 4, 4)):
    # Prepare all images for search
    feats = []
    for img in im_db:
        img = imread(imageRep + img)

        # resize image
        img = center_crop_image(img)
        # Change image color space from RGB to HSV.
        # HSV color space was designed to more closely align with the way human vision perceive color-making attributes
        img = color.rgb2hsv(img)

        # convert image pixels to [0, 255] range, and to uint8 type
        img_int = img_as_ubyte(img)

        # Extract HoC features
        hist, bin_edges = getHistogramOfColor(img_int, bins=bins)

        # Normalize features
        # We add 1 dimension to comply with scikit-learn API
        hist = np.squeeze(normalize(hist.reshape(1, -1), norm="l2"))

        feats.append(hist)

    # Creating a feature matrix for all images
    feats = np.array(feats)
    # print("Shape of feature matrix: {}".format(feats.shape))
    return feats


# Advanced printer fro checking and printing the querry image and the results by crescent order
def print_checking_hoc(img_query, img_db, k_nearest_indexes, k_nearest_dists):

    nr_fig = len(k_nearest_indexes) + 1
    fig, axes = plt.subplots(1, nr_fig, figsize=(50, 6), sharex=False, sharey=False)

    #start with the query image
    print("Query image: {}".format(img_query))
    img_q = imread(imageQueryRep + img_query)
    img_q = center_crop_image(img_q)

    axes[0].axis('off')
    axes[0].imshow(img_q)
    axes[0].set_title('Input_Image', fontsize=20)

    for i, (img_idx, img_dist) in enumerate(zip(k_nearest_indexes, k_nearest_dists)):
        image_fname = img_db[img_idx]
        img = imread(imageRep + image_fname)
        img = center_crop_image(img)
        print("Image #{} - distance: {}".format(i, img_dist))

        axes[i + 1].axis('off')
        axes[i + 1].imshow(img)
        axes[i + 1].set_title(str(i) + ': ' + image_fname, fontsize=16)

    #plt.show()
    fig.savefig(img_query+'.png')


def hoc_space_search(image_q, img_db, feats, metric, bins=(4, 4, 4)):

    image_q_feat = prep_img_query_hoc(image_q, bins)
    # Prepare all images for search

    print("----> Initializing comparison")
    # Use the implemented function to find the K nearest neighbours on the HoC image vector space.
    k_nearest_indexes, k_nearest_dists = k_neighbours(q=image_q_feat.reshape(1, -1), X=feats, metric=metric, k=10)
    print("----> Finalising comparison")

    # Sorted print of nearest
    print(list(zip(k_nearest_indexes, k_nearest_dists, [images[i] for i in k_nearest_indexes])))

    print_checking_hoc(image_q, img_db, k_nearest_indexes, k_nearest_dists)


######### Histrogram of gradiente processing #########

# Histogram of Oriented Gradients
def getHistogramOfGrandiante(image):
    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16), visualise=True)
    # print("HoG Feature vector shape: {}".format(fd.shape))
    return hog_image


def prep_img_feats_hog(im_db, pixels_per_cell=(32, 32), orientations=8):
    feats = []
    for img in im_db:
        img = imread(imageRep + img)

        # resize image
        img = center_crop_image(img)

        # Convert to grayscale
        img = rgb2gray(img)

        # Extract HoG features
        hist = hog(img, orientations=orientations, pixels_per_cell=pixels_per_cell)

        # Normalize features
        # We add 1 dimension to comply with scikit-learn API
        hist = np.squeeze(normalize(hist.reshape(1, -1), norm="l2"))

        feats.append(hist)

    # Creating a feature matrix for all images
    feats = np.array(feats)
    # print("Shape of feature matrix: {}".format(feats.shape))
    return feats


def prep_img_query_hog(image_q, pixels_per_cell=(32, 32), orientations=8):
    img_q = imread(imageQueryRep + image_q)
    img_q = center_crop_image(img_q)
    img_q = rgb2gray(img_q)

    hist = hog(img_q, orientations=orientations, pixels_per_cell=pixels_per_cell)

    image_q_feat = np.squeeze(normalize(hist.reshape(1, -1), norm="l2"))
    # print("Image query feature dimension: {}".format(image_q_feat.shape))
    return image_q_feat


def print_checking_hog(img_query, img_db, k_nearest_indexes, k_nearest_dists):

    nr_fig = len(k_nearest_indexes) + 1
    fig, axes = plt.subplots(1, nr_fig, figsize=(50, 6), sharex=True, sharey=True)

    # start with the query image
    print("Query image: {}".format(img_query))

    img_grey = imread(imageQueryRep + img_query)
    img_grey = center_crop_image(img_grey)
    img_grey = rgb2gray(img_grey)

    axes[0].axis('off')
    axes[0].imshow(img_grey, cmap=plt.cm.gray)
    axes[0].set_title('Input_Image', fontsize=20)

    for i, (img_idx, img_dist) in enumerate(zip(k_nearest_indexes, k_nearest_dists)):
        image_fname = img_db[img_idx]
        img = imread(imageRep + image_fname)
        img = center_crop_image(img)
        img = rgb2gray(img)
        print("Image #{} - distance: {}".format(i, img_dist))

        axes[i + 1].axis('off')
        axes[i + 1].imshow(img, cmap=plt.cm.gray)
        axes[i + 1].set_title(str(i) + ': ' + image_fname, fontsize=16)

    #plt.show()
    fig.savefig(img_query + '.png')


def hog_space_search(image_q, im_db, feats, metric, pixels_per_cell=(32, 32), orientations=8):
    image_q_feat = prep_img_query_hog(image_q, pixels_per_cell, orientations)

    print("----> Initializing comparison")
    k_nearest_indexes, k_nearest_dists = k_neighbours(q=image_q_feat.reshape(1, -1), X=feats, metric=metric, k=10)
    print("----> Finalizing comparison")

    print(list(zip(k_nearest_indexes, k_nearest_dists, [images[i] for i in k_nearest_indexes])))

    print_checking_hog(image_q, im_db, k_nearest_indexes, k_nearest_dists)


######### Image categoriztion processing NN #########

# Pre-processes all images for nn image categorization
def prep_images_catgories(images):
    img_list = []
    for img in images:
        # We are loading each image using Keras image and specifying the target size.
        img = image.load_img(imageRep + img, target_size=(224, 224))

        # Then the function img_to_array converts a PIL (library used by Keras) to a numpy array
        x = image.img_to_array(img)

        # A one dimension is added to the the numpy array (224x224x3) becomes (1x224x224x3)
        x = np.expand_dims(x, axis=0)

        # Apply Keras pre-processing steps specific to ResNet-50 model
        # Check the function here for ResNet-50:
        # def preprocess_input(x):
        #     x /= 255.
        #     x -= 0.5
        #     x *= 2.
        #     return x
        x = preprocess_input(x)

        img_list.append(x)

    # Convert from list to ndarray
    return np.vstack(img_list)


def print_checking_image_classification(concepts, im_db):
    tags_from_images = {}
    for img_idx, c in enumerate(concepts):
        tags_picture = ''
        for words in c:
            tags_picture = tags_picture + ' ' + words[1]
        tags_from_images[im_db[img_idx]] = tags_picture
        print("Image #{} - Predicted: {}".format(img_idx, c))
    return tags_from_images


def img_classification(im_db):
    model = ResNet50(weights='imagenet')
    img_array_list = prep_images_catgories(im_db)
    # Feed all images to the model
    preds = model.predict(img_array_list)
    print("Resulting shape of the network output: {}".format(preds.shape))

    # Print images for verifiction
    concepts = decode_predictions(preds, top=5)

    all_Images_Tags = print_checking_image_classification(concepts,im_db)
    return all_Images_Tags


# _____________________________________________TEXT PROCESSING______________________________________________________#

# Clean de text (remove stopWords, remove tokens) and get a BoW
def getBagOfWords(corpus, vocabulary):
    if (vocabulary == ''):
        # Inicialization tokeneizer (remover tokens)
        tknzr = TreebankWordTokenizer()
        # remove stopwords, exclude words that appear less than 3 times, and removes tokens
        vectorizer = CountVectorizer(stop_words="english", min_df=1, binary=False, tokenizer=tknzr.tokenize)
        # texts_bow is a sparse matrix, BoW sparse matrix for the corpus.
        texts_bow = vectorizer.fit_transform(corpus)
        texts_bow = normalize(texts_bow, norm="l2")
    else:
        tknzr = TreebankWordTokenizer()
        vectorizer = CountVectorizer(stop_words="english", min_df=1, binary=False, tokenizer=tknzr.tokenize,
                                     vocabulary=vocabulary)
        texts_bow = vectorizer.fit_transform(corpus)
        texts_bow = normalize(texts_bow, norm="l2")

    return texts_bow, vectorizer

    # IMPORTANTE : You can check  CountVectorizer  documentation  for more pre - processing steps (e.g.lowercasing,
    # Better tokenizer, stemming, etc.) that may be adequate for your use-case


def getVocabulary(corpus):
    tknzr = TreebankWordTokenizer()
    vect = CountVectorizer(stop_words="english", min_df=2, binary=False, tokenizer=tknzr.tokenize)
    texts_bow = vect.fit_transform(corpus)
    vocabulary = vect.vocabulary_
    return vocabulary

    # texts_bow is a sparse matrix, BoW sparse matrix for the corpus.


def getTextQuerys(corpus):
    fullText = []
    for story in corpus:
        temp = ''
        for segment in story:
            temp = temp + ' / ' + segment
        fullText.append(temp)
    return fullText
    # juntar a lista de listas num texto


# Perform NER on the input sentence.
# First the sentence is tokenized, then parts-of-speech are identified, and then we call the NER function ne_chunk.
def extract_named_entities(corpus):
    #nltk.download()
    ners = []
    for sent in nltk.sent_tokenize(corpus):
        for chunk in nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sent))):
            if hasattr(chunk, 'label'):
                # ners.append(chunk.label())
                ners.append((' '.join(c[0] for c in chunk)))
    return ners


# IF bagOfEnteties_Flag = True ....stories_query = 1 query
# IF bagOfEnteties_Flag = FALSE ....stories_query = list query
def search_bow_querys(tweetsCorpus, corpusBoW, vectorizer, stories_query, bagOfEnteties_Flag, metric):
    if bagOfEnteties_Flag == False:
        print('------------------------------')
        print('Corpus of all tweets :')
        print(tweetsCorpus)
        print('BoW corresponding to tweets :')
        print(corpusBoW)
        print('------------------------------')
        for s in stories_query:
            for q in s:
                # Transform each query in a BoW representation
                queryBoW = vectorizer.transform([q])
                queryBoW = normalize(queryBoW, norm="l2")
                print('Query : ' + q)
                print('BoW corresponding to query :')
                print(queryBoW)
                print('Search on Bag of Words:')
                # Query the database using the prepared query:¶
                k_nearest_indexes, k_nearest_dists = k_neighbours(q=queryBoW, X=corpusBoW, metric=metric, k=10)
                print(list(zip(k_nearest_indexes, k_nearest_dists, [tweetsCorpus[i] for i in k_nearest_indexes])))
                print('------------------------------')
    else:
        queryBoW = vectorizer.transform([stories_query])
        queryBoW = normalize(queryBoW, norm="l2")
        print('BoW of NER corresponding to query :')
        print(queryBoW)
        print('Search on Bag of Words:')
        # Query the database using the prepared query
        k_nearest_indexes, k_nearest_dists = k_neighbours(q=queryBoW, X=corpusBoW, metric="cosine", k=10)
        print(list(zip(k_nearest_indexes, k_nearest_dists, [tweetsCorpus[i] for i in k_nearest_indexes])))
        print('------------------------------')

# ___________________________________LABEL PROPAGATION PROCESSING____________________________________________#
def expand_selected_feature(name_space, X):

    if name_space == "c":
        print("----> Initializing all images HOC features preparation")
        features = prep_img_feats_hoc(X[:, 1])
        print("----> Finish all images HOC features preparation")

        return features

    elif name_space == "g":
        print("----> Initializing all images HOG features preparation")
        features = prep_img_feats_hog(X[:, 1])
        print("----> Finalizing all images HOG features preparation")
        return features

    elif name_space == "t":
        print()
        # Já vai

    elif name_space == "w":

        vocab = getVocabulary(X[:, 0])
        # Train and BoW representation of all tweets
        corpusBoW, _ = getBagOfWords(X[:, 0], vocab)

        return corpusBoW


    elif name_space == "e":

        tweets_corp = X[:, 0]
        print('Starting NeR....')
        bag_of_enteties_corpus = []

        for tweet in tweets_corp:
            text_tweet = ''
            for entity in extract_named_entities(tweet):
                text_tweet = text_tweet + ' ' + entity
                bag_of_enteties_corpus.append(text_tweet)

        vocab = getVocabulary(bag_of_enteties_corpus)
        corpusBoW, _ = getBagOfWords(tweets_corp, vocab)
        return corpusBoW


def build_label_matrix():
    label_list = getCorpusLabels(csv_nameFile)
    tmp_list = []

    for x in label_list:
        tempLabels = x.split(',')
        tempLabels = [int(i) for i in tempLabels]
        tmp_list.append(tempLabels)

    return tmp_list


def organize_label_matrix(labels, indices):
    tmp_list = []

    for i in range(len(labels)):
        tmp_list.append(labels[indices[i]])

    return tmp_list


def build_w_matrix(features_weight, features, metric):
    print("----> Start build W matrix")
    # It haves one feature
    if len(features_weight) == 1:
        w_matrix = pairwise_distances(features[0], features[0], metric=metric)
        sigma = np.std(w_matrix)
        print('Value for Sigma : ')
        print(sigma)
        w_norm = np.zeros_like(w_matrix)
        for i, row in enumerate(w_norm):
            for j, elem in enumerate(row):
                w_norm[i][j] = gaussian_normalization(w_matrix[i][j], sigma)
        print("----> End build W matrix")
        return w_norm

    # It haves two features
    elif len(features_weight) == 2:

        w_matrix_first = pairwise_distances(features[0], features[0], metric=metric)
        w_matrix_second = pairwise_distances(features[1], features[1], metric=metric)
        w_norm = np.zeros_like(w_matrix_first)

        for i, row in enumerate(w_norm):
            for j, elem in enumerate(row):
                # Formula bonita na linha abaixo
                w_norm[i][j] = features_weight[0] * w_matrix_first[i][j] + features_weight[1] * w_matrix_second[i][j]

        sigma = np.std(w_norm)
        print('Value for Sigma : ')
        print(sigma)

        for i, row in enumerate(w_norm):
            for j, elem in enumerate(row):
                w_norm[i][j] = gaussian_normalization(elem, sigma)

        print("----> End build W matrix")
        return w_norm

    else:
        return None


def gaussian_normalization(numerator, sig):
    return np.exp(-(numerator**2/(2*(sig**2))))


def normalize_w_matrix(w_matrix):
    print("----> Start W matrix normalization")
    d_matrix = np.zeros((w_matrix.shape[0], w_matrix.shape[1]))

    for i, row in enumerate(w_matrix):
        tmp = sum(row)
        d_matrix[i][i] = tmp

    inv_d_matrix = np.linalg.inv(d_matrix)

    s_matrix = inv_d_matrix.dot(w_matrix)
    s_matrix = s_matrix.dot(inv_d_matrix)
    print("----> Finished W matrix normalization")
    return s_matrix


def lp_propagation_step(alpha, s_matrix, current_f_matrix, original_f_matrix):

    new_gen_matrix = alpha*s_matrix.dot(current_f_matrix) + (1 - alpha)*original_f_matrix
    return new_gen_matrix


# -------------------------------------------------------------------------------------------------#
#################________________________ TESTES__________________________________##################
# -------------------------------------------------------------------------------------------------#

csv_nameFile = "visualstories_edfest_2016_twitter_xmedia.csv"

# q_images, q_text = getJson_Data(jason_data, False)


print("-> Available functionalities:")
print("--> c: Colors, g: Gradient, t: Image tags, w: Bag of Words, e: Bag of Entities, l: Label Propagation, q: Quit")

se = input(">>")

while se != "q":

    if se == "c":
        m = select_dist_func()
        stories = getJson_Data(jason_data, False)
        images = getImages(csv_nameFile)
        print("----> Initializing all images features preparation")
        feats = prep_img_feats_hoc(images)
        print("----> Finish all images features preparation")
        for h in stories:
            for q in h:
                hoc_space_search(q + ".jpg", images, feats, m)

    elif se == "g":
        m = select_dist_func()
        stories = getJson_Data(jason_data, False)
        images = getImages(csv_nameFile)

        print("----> Initializing all images features preparation")
        feats = prep_img_feats_hog(images)
        print("----> Finalizing all images features preparation")

        for h in stories:
            for q in h:
                hog_space_search(q + ".jpg", images, feats, m)

    elif se == "t":

        # Get all the tweets and bow associated with them
        tweetsCorpus = getCorpusTextFile(csv_nameFile)
        corpusBoW, vectorizerTweets = getBagOfWords(tweetsCorpus, '')

        # Get querys from jason
        stories_querys = getJson_Data(jason_data, False)
        # Processing for imageClassif
        temp = []
        # Necessario colocar o extensao .jpg no nome das imagens para ser usado posterioemente
        for story in stories_querys:
            for segment in story:
                temp.append(segment + '.jpg')

        # Images_tags � um dicionario com em que a key � o nome da imagem e o value as tags associadas a esta imagem
        images_Tags = img_classification(temp)
        # Para todas as historias, ir buscar todas as suas imagens e concat das suas tags para fazer a procura
        for story in stories_querys:
            imageTags_segmets = ''
            print('Query images to analyse : ' + str(story))
            for segment in story:
                imageTags_segmets = imageTags_segmets + ' ' + images_Tags[segment + '.jpg']

            print('Tags of all story images : ' + imageTags_segmets)
            search_bow_querys(tweetsCorpus, corpusBoW, vectorizerTweets, imageTags_segmets, True)

    elif se == "w":
        # Get all the tweets
        m = select_dist_func()
        tweetsCorpus = getCorpusTextFile(csv_nameFile)
        # Get querys from jason
        stories_querys = getJson_Data(jason_data, True)
        textQ = getTextQuerys(stories_querys)

        textToAnalyse = tweetsCorpus + textQ
        vocabulary = getVocabulary(textToAnalyse)
        # Train and BoW representation of all tweets
        corpusBoW, vectorizerTweets = getBagOfWords(tweetsCorpus, vocabulary)

        search_bow_querys(tweetsCorpus, corpusBoW, vectorizerTweets, stories_querys, False, m)

    elif se == "e":
        m = select_dist_func()
        tweetsCorpus = getCorpusTextFile(csv_nameFile)

        print('Starting NeR....')
        bagOfEnteties_Corpus = []
        bagOfEnteties_Querys = []

        for tweet in tweetsCorpus:
            text_tweet = ''
            for entity in extract_named_entities(tweet):
                text_tweet = text_tweet + ' ' + entity
            bagOfEnteties_Corpus.append(text_tweet)
        print('BOW for corpus(tweets) : ')
        print(tweetsCorpus)

        stories_querys = getJson_Data(jason_data, True)
        for story in stories_querys:
            for segment in story:
                text_segmet = ''
                # if(extract_named_entities(segment)!=[]):
                for entity in extract_named_entities(segment):
                    text_segmet = text_segmet + ' ' + entity
                bagOfEnteties_Querys.append(text_segmet)
        print('NER for corpus(querys) : ')
        print(bagOfEnteties_Querys)

        textToAnalyse = tweetsCorpus + bagOfEnteties_Querys
        vocabulary = getVocabulary(textToAnalyse)

        corpusBoW, vectorizer = getBagOfWords(tweetsCorpus, vocabulary)
        print('BoW from Tweets : ')
        print(corpusBoW)
        print('------------------------------')

        for story in stories_querys:
            for segment in story:
                bagOfEnteties_segmet = ''
                print('Query to analyse : ' + segment)
                print('Corresponding NER : ' + str(extract_named_entities(segment)))
                # if(extract_named_entities(segment)!=[]):
                for entity in extract_named_entities(segment):
                    bagOfEnteties_segmet = bagOfEnteties_segmet + ' ' + entity
                if (bagOfEnteties_segmet == ''):
                    print('NER is empty, not searching')
                    print('------------------------------')
                else:
                    search_bow_querys(tweetsCorpus, corpusBoW, vectorizer, bagOfEnteties_segmet, True, m)

    elif se == "l":

        ############ Data connection ############

        # Raw data haves first column of tweets second on image names
        tweets_text = getCorpusTextFile(csv_nameFile)
        tweets_image = getImages(csv_nameFile)

        raw_data = np.column_stack((tweets_text, tweets_image))

        #print(raw_data)
        #print(len(raw_data[:, 0]))
        labels = build_label_matrix()

        ############ Param Initialization ############

        percent, num_iteration, alpha, sigma = select_lp_param()
        m = select_dist_func()
        ns_selection = select_lp_name_spaces_and_weights()

        ############ Data selection ############
        # Get and shuffle indexes
        indices = np.arange(len(raw_data[:, 0]))
        shuffle(indices)

        # Organize data by shuffled order
        X = raw_data[indices][:]
        y_target = organize_label_matrix(labels, indices)


        # Divide data into label and unlabeled
        labeled_set_size = int(X.shape[0] * percent)
        indices_labeled = indices[:labeled_set_size]
        indices_unlabeled = indices[labeled_set_size:]
        #print("Total images labeled: {} - Total images unlabeled: {}".format(len(indices_labeled), len(indices_unlabeled)))


        mb1 = MultiLabelBinarizer()
        # Keep groundtruth labels
        Y_true = mb1.fit_transform(y_target)



        # Duplicate data for train and remove their labels
        f_matrix_org = mb1.fit_transform(y_target)
        f_matrix_org[indices_unlabeled, :] = np.zeros(f_matrix_org.shape[1])
        f_matrix_org = normalize(f_matrix_org, norm="l1")


        ############ Feature expansion handler ############

        features_weight = []
        features = []

        if ns_selection[0] != 0:
            features_weight.append(ns_selection[0])
            features.append(expand_selected_feature("w", X))

        if ns_selection[1] != 0:
            features_weight.append(ns_selection[1])
            features.append(expand_selected_feature("e", X))

        if ns_selection[2] != 0:
            features_weight.append(ns_selection[2])
            features.append(expand_selected_feature("c", X))

        if ns_selection[3] != 0:
            features_weight.append(ns_selection[3])
            features.append(expand_selected_feature("g", X))


        ############ Computation ############
        w_matrix = build_w_matrix(features_weight, features, m)
        s_matrix = normalize_w_matrix(w_matrix)
        f_matrix_iterated = lp_propagation_step(alpha, s_matrix, f_matrix_org, f_matrix_org)
        f_matrix_iterated = normalize(f_matrix_iterated, norm="l1")

        for index in range(num_iteration-1):
            f_matrix_iterated = lp_propagation_step(alpha, s_matrix, f_matrix_iterated, f_matrix_org)
            f_matrix_iterated = normalize(f_matrix_iterated, norm="l1")

        #  print(f_matrix_iterated)

        #EVALUATION
        # Get the predictions of the unlabeled documents
        temp_Y_pred = f_matrix_iterated[indices_unlabeled, :]
        #Y_pred = f_matrix_iterated[indices_unlabeled, :]

        # FAZER O ARGMAX PARA SELECIONAR AS LABELS

        #Discuss examples of thresholds (e.g. select the top-3 labels, keep all categories with their values $&gt;\alpha$, etc.)
        selection_type = False
        k_best = 3


       #defenir o treshHold
        tempMean=0
        tempDeviaton = 0
        for pred in temp_Y_pred:
            tempMean = tempMean + np.mean(pred)
            tempDeviaton = tempDeviaton + np.std(pred)

        threshHold_mean = tempMean / len(temp_Y_pred)
        threshHold_plus = (tempMean / len(temp_Y_pred)) + tempDeviaton / len(temp_Y_pred)
        threshHold_minus = (tempMean / len(temp_Y_pred)) - tempDeviaton / len(temp_Y_pred)

        print('Possible thresholds:')
        print('Mean : ' + str(threshHold_mean))
        print('Mean + Deviation : ' + str(threshHold_plus))
        print('Mean - Deviation : ' + str(threshHold_minus))

        threshHold = threshHold_plus

        #Para cada tweet que previmos a label:
        temp_target = []
        for pred in temp_Y_pred:

            best_ind = []

            #Vamos buscar o valor dos indices das 3  labels com o maior valor.
            if(selection_type == True) :
                for q in range (0,k_best):
                    best_ind.append(np.argmax(pred))
                    #Por o valor a 0 para este indice nao voltar a ser escolhido
                    pred[np.argmax(pred)] = 0
                temp_target.append(best_ind)

            else:
                tempTresh = pred[np.argmax(pred)]
                best_ind.append(np.argmax(pred))
                pred[np.argmax(pred)] = 0

                while tempTresh > threshHold :
                    tempTresh = pred[np.argmax(pred)]
                    best_ind.append(np.argmax(pred))
                    # Por o valor a 0 para este indice nao voltar a ser escolhido
                    pred[np.argmax(pred)] = 0
                temp_target.append(best_ind)


        #Criar os vectores como se fez anteriormente
        # Temos que defenir um novo MuiltiBina, por que em certos casos, não existe todoa as diferentes labels na amosta
        mb2 = MultiLabelBinarizer([0,1,2,3,4,5,6,7,8,9,10,11,12])
        Y_pred = mb2.fit_transform(temp_target)
        #print(Y_pred)
        # Get the corresponding groundtruth
        y_gt = Y_true[indices_unlabeled, :]
        #print(y_gt.shape)

        print(classification_report(y_gt, Y_pred))

        #Continuar para ler dados e erros
    else:
        print("!!!!!Wrong command!!!!!")

    print("-> Available functionalities:")
    print("--> c: Colors, g: Gradient, t: Image tags, w: Bag of Words, e: Bag of Entities, l: Label Propagation, q: Quit")

    se = input(">>")

print("Bye")

# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 09:38:43 2017

@author: Andre-X
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 08:09:18 2017

@author: rf.afonso
"""
import numpy as np
import math
import matplotlib.pyplot as plt
from skimage.io import imsave,imread
from sklearn.cluster import KMeans
from pandas.tools.plotting import scatter_matrix
from pandas import read_csv
from mpl_toolkits.mplot3d import axis3d
from sklearn.neighbors import KNeighborsClassifier 


import itertools
from sklearn.metrics import silhouette_score
from sklearn.metrics import adjusted_rand_score
from sklearn import cluster, mixture
from sklearn.cluster import DBSCAN
from sklearn.mixture import GaussianMixture



RADIUS = 6371

def plot_classes(labels,lon,lat, alpha=0.5, edge = 'k'):
    """Plot seismic events using Mollweide projection.
    Arguments are the cluster labels and the longitude and latitude
    vectors of the events"""
    img = imread("Mollweide_projection_SW.jpg")        
    plt.figure(figsize=(10,5),frameon=False)    
    x = lon/180*np.pi
    y = lat/180*np.pi
    ax = plt.subplot(111, projection="mollweide")
    print(ax.get_xlim(), ax.get_ylim())
    t = ax.transData.transform(np.vstack((x,y)).T)
    print(np.min(np.vstack((x,y)).T,axis=0))
    print(np.min(t,axis=0))
    clims = np.array([(-np.pi,0),(np.pi,0),(0,-np.pi/2),(0,np.pi/2)])
    lims = ax.transData.transform(clims)
    plt.close()
    plt.figure(figsize=(10,5),frameon=False)    
    plt.subplot(111)
    plt.imshow(img,zorder=0,extent=[lims[0,0],lims[1,0],lims[2,1],lims[3,1]],aspect=1)        
    x = t[:,0]
    y= t[:,1]
    nots = np.zeros(len(labels)).astype(bool)
    diffs = np.unique(labels)    
    ix = 0   
    for lab in diffs[diffs>=0]:        
        mask = labels==lab
        nots = np.logical_or(nots,mask)        
        plt.plot(x[mask], y[mask],'o', markersize=4, mew=1,zorder=1,alpha=alpha, markeredgecolor=edge)
        plt.plot()
        ix = ix+1                   
    mask = np.logical_not(nots)    
    if np.sum(mask)>0:
        plt.plot(x[mask], y[mask], '.', markersize=1, mew=1,markerfacecolor='w', markeredgecolor=edge)

    plt.axis('off')
    plt.title("Best Parameter")
    plt.savefig("Gaussian-176.png", dpi=400, bbox_inches='tight')



def rand_index(labels,faults,pairs):
    TP=0
    TN=0
    FP=0
    FN=0
    
    faults_aux0 = np.array(faults[pairs[:,0]])
    faults_aux1 = np.array(faults[pairs[:,1]])
    labels_aux0 = np.array(labels[pairs[:,0]])
    labels_aux1 = np.array(labels[pairs[:,1]])
    
    equal_faults = faults_aux0 == faults_aux1
    dif_faults = faults_aux0 != faults_aux1
    equal_labels = labels_aux0 == labels_aux1
    dif_labels = labels_aux0 != labels_aux1
    
    #faults[pair[0]] == faults[pair[1]] and labels[pair[0]] == labels[pair[1]]
    TP = np.sum(equal_faults[np.where(equal_labels==True)[0]])
    
    #faults[pair[0]] != faults[pair[1]] and labels[pair[0]] != labels[pair[1]]
    TN = np.sum(dif_faults[np.where(dif_labels == True)[0]])
    
    #faults[pair[0]]!=faults[pair[1]] and labels[pair[0]] == labels[pair[1]]
    FP = np.sum(dif_faults[np.where(equal_labels == True)[0]])
    
    #faults[pair[0]]==faults[pair[1]] and labels[pair[0]] != labels[pair[1]]
    FN = np.sum(equal_faults[np.where(dif_labels == True)[0]])
               
    
    Rand = (TP+TN)/(TP+TN+FP+FN)    
    adjusted_rand = adjusted_rand_score(faults, labels)
    Precision = TP/(FP+TP)
    Recall = TP/(FN+TP)
    F1 = 2*(Precision*Recall)/(Precision+Recall)   
    
    return Rand, adjusted_rand, Precision, Recall, F1


    
def transform_coordinates(latitude, longitude):            
    x = RADIUS * math.cos(latitude * math.pi/180) * math.cos(longitude * math.pi/180)
    y = RADIUS * math.cos(latitude * math.pi/180) * math.sin(longitude * math.pi/180) 
    z = RADIUS * math.sin(latitude * math.pi/180)
    
    return (x,y,z)


def calc_KMeans(xyz, k):
    #cria um KMeans com k centroids
    kmeans = KMeans(n_clusters=k).fit(xyz)
    labels = kmeans.predict(xyz)
 
    centroids = kmeans.cluster_centers_
  
    c_xyz = np.zeros(xyz.shape)
    for ix in range(xyz.shape[0]):
        
        c_xyz[ix,:]=centroids[labels[ix]]

    return labels,centroids


def calc_Eps(points):
    y = np.zeros(xyz.shape[0])
    KNN=KNeighborsClassifier(n_neighbors=4)
    KNN.fit(points, y)
    neigh_dist = []
    
    # o metodo é: para todos os pontos ves o 4 vizinhos mais proximos, por isso é usado o 
    #KNeighbors para encontrar esses 4 vizinhos. Com esses 4 vizinhos fazemos sort deles e escolhemos
    #o que está mais longe ( que é o neigh[0][0][3])
    for i in range(points.shape[0]):
        neigh = KNN.kneighbors([points[i,:]])
        np.sort(neigh)
        neigh_dist.append((i,neigh[0][0][3]))
   
    neigh_dist = np.asarray(neigh_dist)
    
    #depois é so odenar o array do maior para o menor e fazer plot
    #e encontrar mais ou menos o melhor Eps
    neigh_dist = neigh_dist[np.argsort(neigh_dist[:,1])]
    neigh_dist = neigh_dist[::-1]
    
    plt.figure(figsize=(8,6))
    plt.title("Eps")
    plt.plot(np.arange(points.shape[0]) ,neigh_dist[:,1], 'b')
    plt.savefig("Eps.png", dpi=200, bbox_inches='tight')
    plt.show    
    
    
#faz o plot de todos os indexes
def plot_indexes(rand_ix,title):   
    plt.figure(figsize=(8,6))
    plt.title(title)
    plt.plot(rand_ix[:,0],rand_ix[:,1], 'r', label="Rand") #Rand
    plt.plot(rand_ix[:,0],rand_ix[:,2], 'y', label="Adjusted_Rand") #Adjusted_Rand
    plt.plot(rand_ix[:,0],rand_ix[:,3], 'b', label="Precision") #Precision
    plt.plot(rand_ix[:,0],rand_ix[:,4], 'k', label="Recall") #Recall
    plt.plot(rand_ix[:,0],rand_ix[:,5], 'g', label="F1") #F1
    plt.plot(rand_ix[:,0],rand_ix[:,6], 'm', label="Silhouette") #silhouette_score
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.savefig(title+".png", dpi=200,bbox_inches='tight')
    plt.show


data = read_csv("tp2_data.csv")
coordinates = data[['latitude','longitude']]
faults = data.fault
#converte as coordenadas de latitude/longitude para x y z
xyz= []
for i in range(data.shape[0]):
    xyz.append(transform_coordinates(coordinates.iloc[i,0],coordinates.iloc[i,1]))
xyz = np.asarray(xyz)

    

#cria todas as combinações de pares de pontos
#para poder calcular os index da aula 20 slide 35 e 36
pairs = list(itertools.combinations(range(data.shape[0]),2))
pairs = np.asarray(pairs)


print("KMeans")
rand_ix=[]
aux = []
for k in range(2,36,2):
    print(k)
    labels,centroids = calc_KMeans(xyz,k)
    Sil = silhouette_score(xyz,labels)
    Rand,Ad_rand, Precision, Recall, F1 = rand_index(labels,faults,pairs)
    rand_ix.append((k,Rand, Ad_rand, Precision, Recall, F1,Sil))
    aux.append(labels)

rand_ix =  np.asarray(rand_ix)

#Melhor paramentro encontrado
for i in range(rand_ix.shape[0]):
    if rand_ix[i,0] == 18:
        labels = aux[i]
        break
    
print("kmean")
plot_indexes(rand_ix, "Indexes KMeans")
plot_classes(labels,data.longitude, data.latitude, alpha=0.5, edge = 'k')



#DBSCAN
print("DBSCAN")
calc_Eps(xyz)
rand_ix=[]
aux = []

for i in range(50,250,5):
    print(i)
    DB = DBSCAN(eps=i, min_samples=4)
    labels = DB.fit_predict(xyz)
    Sil = silhouette_score(xyz,labels)
    Rand,Ad_rand, Precision, Recall, F1 = rand_index(labels,faults,pairs)
    rand_ix.append((i,Rand, Ad_rand, Precision, Recall, F1,Sil))
    aux.append(labels)
    
rand_ix =  np.asarray(rand_ix)
best_sil = 0;

#Melhor paramentro encontrado
for i in range(rand_ix.shape[0]):
    if rand_ix[i,0] == 130:
        labels = aux[i]
        break
       
print("dbscan")
plot_indexes(rand_ix, "Indexes DBSCAN")
plot_classes(labels,data.longitude, data.latitude, alpha=0.5, edge = 'k')


###GAUSSIAN
print("GAUSSIAN")
rand_ixBD=[]
aux=[]
for l in range(2,36,2):
    print(l)
    GM = GaussianMixture(n_components=l,covariance_type='diag')
    GM.fit(xyz)
    labels = GM.predict(xyz)
    Sil = silhouette_score(xyz,labels)
    Rand,Ad_rand, Precision, Recall, F1 = rand_index(labels,faults,pairs)
    rand_ixBD.append((l,Rand, Ad_rand, Precision, Recall, F1,Sil))
    aux.append(labels)

rand_ixBD =  np.asarray(rand_ixBD)

#Melhor paramentro encontrado
for i in range(rand_ixBD.shape[0]):
    if rand_ixBD[i,0] == 14:
        labels = aux[i]
        break
       

plot_indexes(rand_ixBD, "Indexes Gaussian")
plot_classes(labels,data.longitude, data.latitude, alpha=0.5, edge = 'k')


plt.show


﻿Agricultura orgânica
Origem: Wikipédia, a enciclopédia livre.
Text document with red question mark.svg
Este artigo ou secção contém fontes no fim do texto, mas que não são citadas no corpo do artigo, o que compromete a confiabilidade das informações. (desde dezembro de 2009)
Por favor, melhore este artigo inserindo fontes no corpo do texto quando necessário.	

Plantação de batatas orgânicas.
Agricultura orgânica e agricultura biológica são expressões frequentemente usadas para designar sistemas sustentáveis de agricultura que não permitem o uso de produtos químicos sintéticos prejudiciais para a saúde humana e para o meio ambiente, tais como alguns tipos variados de fertilizantes e agrotóxicos sintéticos,[1][2] nem de organismos geneticamente modificados.

Os seus proponentes acreditam que, num solo saudável, mantido sem o uso de fertilizantes químicos sintéticos e agrotóxicos não-orgânicos, os alimentos têm qualidade superior à de alimentos convencionais. Diversos países, incluindo os Estados Unidos (NOP - National Organic Program), o Japão (JAS - Japan Agricultural Standard), a Suíça (BioSuisse), a União Europeia (CEE 2092/91), a Austrália (AOS - Australian Organic Standard / ACO - Australia Certified Organic) e o Brasil (ProOrgânico - Programa de Desenvolvimento da Agricultura[3]), já adotaram programas padrões para a regulação e desenvolvimento desta atividade.

Este sistema de produção, que exclui o uso de fertilizantes sintéticos, agrotóxicos não-orgânicos e produtos reguladores de crescimento, tem, como base: o uso de fertilizantes naturais; a manutenção do solo protegido dos raios solares e das gotas de chuva; a rotação de culturas; o aumento da biodiversidade; consorciação de culturas; adubação verde; compostagem; e controle biológico de insetos e doenças.[4] Pressupõe, ainda, a manutenção da estrutura e da profundidade do solo, sem alterar suas propriedades por meio do uso de produtos químicos sintéticos.

Índice 
1	Princípios[5]
2	Características
2.1	Diferenças nutricionais entre convencional e orgânico
2.2	Diferenças econômicas entre convencional e orgânico
2.3	Resíduos de agrotóxicos
2.4	Movimentos de promoção da alimentação orgânica no Brasil
3	O movimento orgânico e suas subdivisões
4	Problemas e desafios
4.1	Certificação dos orgânicos
5	Ver também
6	Referências
7	Ligações externas
Princípios[5]
O solo é um organismo vivo, e deve ser tratado com o máximo de cuidado possível para manter toda a vida nele existente;
Uso de adubos orgânicos de baixa solubilidade;
Controle de insetos e doenças com medidas preventivas e produtos naturais;
O mato (ervas daninhas) faz parte do sistema. Deve ser usado como cobertura de solo e abrigo de insetos.
Características
O princípio da produção orgânica é o estabelecimento do equilíbrio da natureza utilizando métodos naturais de adubação e de controle de pragas.[5]

O conceito de alimentos orgânicos não se limita à produção agrícola, estendendo-se também à pecuária (em que o gado deve ser criado sem remédios alopáticos ou hormônios), bem como ao processamento de todos os seus produtos: alimentos orgânicos industrializados também devem ser produzidos sem produtos químicos artificiais, como os corantes e aromatizantes artificiais.

Pode-se resumir a sua essência filosófica em desprezo absoluto por tudo que tenha origem na indústria química.[carece de fontes] Todas as demais indústrias (mecânica, energética, logística) são admissíveis, desde que não muito salientes.

A cultura de produtos orgânicos não se limita a alimentos. Há uma tendência de crescimento no mercado de produtos orgânicos não alimentares,[carece de fontes] como fibras orgânicas de algodão (para serem usadas na produção de vestes). Os proponentes das fibras orgânicas dizem que a utilização de pesticidas em níveis excepcionalmente altos, além de outras substâncias sintéticas, na produção convencional de fibras, representa abuso ambiental por parte da agricultura convencional.[carece de fontes]

A pedologia limitou-se durante décadas ao estudo da estrutura físico-química do solo. Hoje, a agronomia se ressente de seu desconhecimento da microfauna e microflora do solo e sua ecologia. Estima-se que 95% dos micro-organismos que vivem no solo sejam desconhecidos pela ciência.[carece de fontes]

Muitos estados dos Estados Unidos agora oferecem certificação orgânica para seus fazendeiros.[carece de fontes] Para um sistema de produção ser certificado como orgânico, a terra deve ter sido usada somente com métodos de produção orgânica durante um certo período de anos antes da certificação.

No Reino Unido, a certificação orgânica é realizada por algumas organizações, das quais as maiores são a Soil Association e a Organic Farmers & Growers. Todos os organismos certificadores estão sujeitos aos regulamentos da Penitente King dom Registes of Organic Food Standards, ligado à legislação da União Europeia. Na Suécia, a certificação orgânica é realizada pela Krav. Na Suíça, o controle é feito pelo Instituto Biodinâmico.[carece de fontes]

Diferenças nutricionais entre convencional e orgânico
Os movimentos relacionados tanto à produção quanto ao consumo de alimentos orgânicos atribuem como uma das vantagens desses a alegação de terem diferenças nutricionais significativas em sua composição quando comparados com alimentos oriundos de sistemas convencionais. Desde então, várias pesquisas científicas foram feitas com objetivo de avaliar a ocorrência dessas diferenças. Em geral, são relatados aumentos na composição nutricional de alimentos de origem orgânica, especialmente na quantidade de micronutrientes, vitaminas e compostos bioativos[6]. Em alguns casos, no entanto, não são observadas diferenças significativas. Segue uma tabela com alguns exemplos de estudos científicos que mostram diferenças de composição entre alimentos orgânicos e convencionais:

Tabela 1: Diferenças nutricionais relatadas em artigos científicos entre alimentos orgânicos e convencionais.

Nutriente	Diferenças relatadas
Proteínas	Acréscimo de 25-30% de lisina em grão de trigo orgânico[7] [8].
Lipídeos	Menos gordura saturada e mais ácidos graxos poliinsaturados em gado bovino orgânico.
Maior nível de ômega-3 em frango orgânico[9].

Minerais	Maiores níveis de ferro e magnésio em legumes orgânicos[6].
Maiores níveis de cobre, zinco e cálcio em cevada orgânica[10].

Acréscimo de 21% de ferro e 29% de magnésio em cultivos vegetais orgânicos[11].

Vitaminas e derivados	Maiores níveis de vitamina C em variedades orgânicas de batatas, tomates, aipo e couve[6].
Maiores níveis de vitamina E em vegetais orgânicos e maiores níveis de b-caroteno em tomates orgânicos[6].

Fitoquímicos	Maiores níveis de polifenóis em maçãs, pêssegos, peras, batatas, cebolas, tomates, pimentas, laranjas e azeites de oliva[6].
Aumento de resveratrol em vinhos orgânicos[12].

A priori, essa maior qualidade nutricional atribuída aos alimentos orgânicos deve-se a um conjunto de fatores tais como[13].

maior dedicação dos agricultores nos sistemas de produção natural;
necessidade de otimizar as condições de fertilidade do solo para garantir o crescimento das espécies vegetais que serão consumidas diretamente ou servirão de insumo para alimentação de animais;
produção planejada de acordo com as condições de clima, solo e biodiversidade local, possibilitando melhor qualidade na produção de alimentos;
menor presença (ou ausência) de substâncias interferentes adicionadas que possam causar prejuízos metabólicos nos organismos participantes da cadeia alimentar, tais como fertilizantes químicos e agrotóxicos.
Diferenças econômicas entre convencional e orgânico
Produtos orgânicos oferecem uma maior garantia para o consumidor quando se trata de saúde e origem do que está sendo consumido, levando, assim, a uma preferência no consumo, além da sustentabilidade no quesito ambiental, social e ético proporcionada pela prática da agricultura orgânica. Portanto, na esfera socioeconômica, eles acabam apresentando um maior valor quando comparados a alimentos convencionais, fato que pode ser observado no preço desses alimentos, que acabam sendo mais elevados, devido ao respeito ao ambiente, produtor e consumidor deles. Isso mostra a necessidade de uma regulamentação da atividade orgânica no Brasil, junto a um incentivo da produção orgânica, com legislação que apoie esses produtores, possibilitando uma competitividade com a agricultura convencional de produção de alimentos em massa.

Resíduos de agrotóxicos
Políticas neoliberais no Brasil, com foco em exportação de commodities agrícolas, fez do país um dos maiores consumidores de agrotóxicos no mundo. O uso crescente de agrotóxicos e a perda do controle estatal quanto ao registro desses, desprotege a população de seus efeitos nocivos, sobretudo aqueles que se encontram em maior risco de contaminação, como trabalhadores e moradores de zonas rurais. A função dos agrotóxicos na agricultura convencional é possibilitar a grande produção, evitando pragas e doenças nas plantações. A aplicação indiscriminada e sem regulamentação do Estado leva à contaminação dos solos e dos recursos hídricos. Além da degradação ambiental, os alimentos em contato com esses produtos químicos podem apresentar algum grau de contaminação. O consumo de tais alimentos pode gerar acúmulo de defensivos agrícolas no organismo, acarretando em intoxicações agudas ou crônicas, que apresentam quadros variados que vão desde alergias, náuseas e vômitos a neoplasias, lesões hepáticas e cânceres[14].

Movimentos de promoção da alimentação orgânica no Brasil
No Brasil, o uso de agrotóxicos em grande escala e de forma indiscriminada tem gerado muitos prejuízos ao meio ambiente e à saúde humana. Isso já faz algumas décadas,  desde o início da Revolução Verde. Com isso, tem-se observado também o nascimento de vários movimentos ligados à produção orgânica de alimentos com enfoque em regimes de agricultura familiar, consumo da produção local, minimização de poluentes, manutenção da variedade genética, sustentabilidade e valorização de alimentos da época[15]. Dentre esses grupos, podem ser citados os seguintes movimentos e organizações: Associação Brasileira de Agroecologia (ABA), Movimento Urbano de Agroecologia de São Paulo (MUDA-SP), Associação de Agricultura Orgânica (AAO), Movimento dos Trabalhadores Rurais Sem Terra (MST), Fundação Mokiti Okada, Campanha Permanente Contra os Agrotóxicos e Pela Vida entre outros.

O movimento orgânico e suas subdivisões
A expressão "agricultura orgânica" não é visto com unanimidade, nem parece ter um significado etimologicamente correto, mas tornou-se reconhecido como sinônimo de "agricultura mais perto da natureza". Não se refere, porém, a um único método de agricultura. Há quem diga que se trata mais de uma ideologia do que de um conjunto de técnicas agrícolas.[5]

Entre as correntes que se contrapõem à monocultura convencional e que são, por isto, chamadas de alternativas, estão:[carece de fontes]

Agriculturas orgânica e biológica: baseadas nas observações que sir Albert Howard fez, no começo do século XX, dos métodos de agricultores indianos. O princípio de sua teoria é que a sanidade vegetal depende do húmus do solo, que se produz na presença dos micro-organismos.[5]
Agricultura biodinâmica: nascida das palestras proferidas por Rudolf Steiner em 1920. Toda a sua teoria baseia-se no princípio de que a sanidade vegetal depende de sua inserção na "matriz energética universal".
Agricultura natural: proposta por Mokiti Okada em 1935. Vê, na reciclagem, que imita os processos da natureza, a base da sanidade vegetal e animal que, de acordo com Okada, é a base da sanidade humana.
Permacultura: desenvolvida em 1975 na Austrália por Bill Mollison. Reúne técnicas tradicionais de vários povos indígenas já extintos, e une-as à integração com a ecologia local, e a ecologia humana.
Agricultura Nasseriana:[16] é uma corrente da agricultura ecológica que tem, como base, a experiência de Nasser Youssef Nasr no Espírito Santo, no Brasil. Também chamada de "biotecnologia tropical", defende o estímulo e manejo de ervas nativas e exóticas, a diversidade de insetos e plantas, a aplicação direta de estercos e resíduos orgânicos na base das plantas, adubações orgânicas e minerais pesadas. Nasser diz que a agricultura de clima tropical do Brasil não precisa de compostagem, pois o clima quente e as reações fisiológicas e bioquímicas intensas garantem a transformação no solo da matéria orgânica. No Brasil, defende Nasser, o esterco deve ser colocado diretamente na planta, pois esta sabe o momento apropriado de lançar suas radículas na matéria orgânica que está em decomposição, e os micro-organismos do solo buscam no esterno os nutrientes necessários para a planta e os levam para baixo da terra. Outro ponto interessante é o uso de ervas nativas e exóticas junto com a cultura para que haja diversidade de inços. Desta forma, é preciso manejar as ervas nativas de maneira que elas mantenham o solo protegido e façam adubação verde. Não temos uma agricultura de solo, mas de sol.
Na prática, essas correntes têm pontos em comum, e suas práticas diárias não diferem significativamente. Fazem, todas elas, parte da mudança de paradigma que está em processo:[carece de fontes] o modelo cartesiano de causa-efeito sendo substituído nas ciências da vida pelo modelo sistêmico.

Problemas e desafios
Estudos encontraram que a toxicidade e impacto ambiental de pesticidas e agrotóxicos de origem orgânica pode superar àquela de produtos sintéticos devido à necessidade de aplicação de uma quantidade maior para obter resultados semelhantes.[17][18]

Certificação dos orgânicos
O mercado de alimentos orgânicos já é responsável por uma fatia considerável do mercado de alimentos, tendo em 2000 superado a marca dos 10% de área cultivada em relação ao total cultivado em alguns países como a Áustria e a Suécia enquanto a estimativa do mesmo ano nos EUA, feita pelo USDA é de crescimento de 12% ao ano no número de produtores de orgânicos.[19] Com um crescimento cada vez maior do seu mercado e, devido ao fato de geralmente ter um custo mais alto de produção e preços de venda mais altos, a venda de alimentos orgânicos está sujeita à fraudes.[20] A certificação de orgânicos é um tema novo no Brasil e há indícios de que uma maior transparência no processo de certificação e maiores dados ao consumidor final poderão aumentar o mercado dos orgânicos no país.[21] Atualmente a certificação dos orgânicos no Brasil é controlada pelo Ministério da Agricultura através do SISORG. Para pertencer ao Cadastro Nacional de Produtores Orgânicos, existem três formas de certificação, são elas: Certificação por Auditoria, Sistema Participativo de Garantia e Controle Social na Venda Direta.[22] A qualidade do produto está relacionada não somente com seu aspecto visual, mas com a reputação do produtor e da certificação.[21]

Métodos analíticos

Existe urgência por técnicas analíticas confiáveis de diagnóstico para verificação de fraudes e estratégias baseadas em espectrometria de massa têm se mostrado muito importantes na análise de alimentos.[23]



Isótopos de nitrogênio[20]: Em geral, nos fertilizantes sintéticos a razão entre os isótopos 15N/14N é muito menor do que a razão nos fertilizantes orgânicos. Isso ocorre porque os sintéticos são produzidos a partir do nitrogênio atmosférico, que é bem menos rico em 15N do que os compostos orgânicos. Esse é um teste com taxa de acerto boa na maior parte dos casos estudados, mas serve exclusivamente para plantas que não sejam fixadoras de nitrogênio (não serve para leguminosas, por exemplo) e preferencialmente para plantas de crescimento rápido. É um teste cujo potencial varia muito de acordo com a planta analisada e com as condições do procedimento agrícola aplicado. Para esse teste é necessário um espectrômetro de massa sensível a ponto de ter resolução para distinguir isótopos.


Composição de elementos químicos[20]: a análise de diversos elementos químicos que compõem a amostra se mostra bastante eficaz, apesar de ser necessária a análise de dezenas de elementos químicos. Devido à grande quantidade de elementos químicos analisados, para poder comparar uma amostra com as demais é necessária a realização de uma abordagem multivariada como PCA ou Rede bayesiana de forma a verificar quais elementos são realmente importantes para a distinção entre os orgânicos e os convencionais. A variação de proporção entre os elementos não tem consistência entre espécies e nem mesmo entre variedades de uma planta, isto é, os resultados obtidos para distinção de uma variedade não serve para outra e, além disso, deve-se levar em consideração o clima e o solo do plantio. Apesar disso, existem evidências crescentes de diferença sistemática na concentração de elementos como Cu, Mn, Ca, e Zn. Há indícios de que essa diferença seja resultante da presença de Micorriza arbuscular em "solos orgânicos".
Exemplos de análises que exibem a composição de elementos químicos são, novamente a espectrometria de massa e a análise do espectro de emissão de raios-X.



Análise de metabólitos e de proteínas[20]: é a análise da quantidade de açúcares, nucleotídeos, álcoois, vitaminas, etc. Ela também exige uma análise estatística multivariada. Apesar disso, os estudos realizados com cereais não fizeram essa estatística e encontraram variações em somente 8 dos 52 metabólitos analisados. A contagem dessas substâncias é feita usando espectrometria de massa e a separação das substâncias feita por cromatografia como a cromatografia gasosa.


Compostos fenólicos[20]: a análise consiste na comparação entre a presença de metabolitos secundários, como os flavonóides e antioxidantes. A explicação apresentada para os maiores níveis de metabólitos secundários nos alimentos orgânicos se dá através da observação de que as plantas orgânicas são expostas a maiores situações de estresse como a presença de animais e larvas, portanto são resultados de mecanismos de defesa. Outra justificativa para a exibição desses compostos é que os alimentos orgânicos geralmente possuem um tempo de amadurecimento e crescimento mais longo e é nesses períodos que a planta produz tais substâncias.
Outras análises para as quais existem menos estudos mas têm exibido grande potencial para a distinção de alimentos orgânicos e convencionais são[20] a análise de compostos voláteis (os compostos que evaporam, como os gases dos sucos de frutas), análise do transcritoma (o funcionamento dos genes das plantas) e a comparação entre as cristalizações do cloreto de cobre em contato com as plantas.

Ver também
Pecuária orgânica
Sistema Brasileiro de Avaliação de Conformidade Orgânica - SISORG
Agricultura biointensiva
Agricultura sustentável
Metabolômica
Proteômica
﻿Moeda privada
Origem: Wikipédia, a enciclopédia livre.

Nota de 10 dólares emitida pelo governo de Norton I, Imperador dos Estados Unidos.
Moeda privada é uma moeda emitida por uma entidade privada, seja ela uma pessoa física, jurídica ou entidades sem fins lucrativos. Muitas vezes, é contrastada com moeda fiduciária emitida por governos ou bancos centrais. Em muitos países, a emissão de moedas privadas é severamente restringida por lei.

Hoje, existem mais de quatro mil moedas privadas em mais de 35 países. Estes incluem transações comerciais na forma de escambo que usam créditos e unidades de troca, trocas privadas de ouro, prata, papel-moeda local; sistemas informatizados de crédito e débito e criptomoedas em circulação, como a DGC (digital gold currency - "moeda digital de ouro").[1]

Notas de bancos privados

Nota emitida pela Mason Barrowman Company da Escócia, em 1764. O valor desta nota é mantido por uma promessa de pagamento. Contém uma cláusula que dá aos diretores a opção não resgatar a nota por demanda, mas seis meses depois, pagando juros nesse período.

Nota no valor de 25 centavos emitida pela cidade de Detroit, EUA, em 1838.

Cédula privada, no valor de 20 Libras, emitida na Austrália pelo City Bank of Sydney, c. 1900.
Nos Estados Unidos, a Free Banking Era durou de 1837 a 1866, quando quase qualquer um podia emitir papel-moeda. Estados, municípios, bancos privados, empresas de transporte ferroviário e de construção, lojas, restaurantes, igrejas e indivíduos imprimiram aproximadamente 8.000 tipos diferentes de dinheiro até 1860. Se o emissor falisse, fechasse, mudasse de localidade ou, de alguma forma, mudasse seu ramo de negócio, a nota tornava-se inútil. Essas organizações obtiveram o apelido de wildcat banks ("bancos gato-selvagem") por uma reputação de falta de confiabilidade; eram muitas vezes situados em locais remotos e despovoados que, dizia-se, eram habitados mais por gatos selvagens do que por pessoas. O National Bank Act de 1863 encerrou o período do wildcat bank.

Na Austrália, o Bank Notes Tax Act 1910 efetivamente encerrou a circulação de moedas privadas, impondo um imposto proibitivo sobre a prática. A Lei foi revogada pelo Commonwealth Bank Act 1945, que impôs uma multa sobre a emissão de moedas privadas.

O Reserve Bank of Australia Act 1959 [2] proibiu esta prática. Em 1976, Wickrema Weerasooria publicou um artigo que sugeria que a emissão de cheques bancários violava esta seção, embora alguns bancos tenham argumentado que, uma vez que os cheques bancários eram impressos com as palavras "não negociáveis" neles, os cheques não eram destinados a circulação e, assim, não violavam o estatuto.[3]

Em Hong Kong, embora o governo emita moeda, as moedas privadas emitidas pelos bancos são o meio de troca dominante. A maioria dos caixas eletrônicos emite cédulas privadas de dólar de Hong Kong.[4]

Na Escócia, o Bank of Scotland, o Clydesdale Bank e o Royal Bank of Scotland, e na Irlanda do Norte, o Bank of Ireland, o Danske Bank, o First Trust Bank e o Ulster Bank, são autorizados pelo parlamento britânico a emitir cédulas de libra esterlina. Eles estão sujeitos aos regulamentos do banco central (Banco da Inglaterra) em relação aos "ativos de apoio vedados" e são apoiados em parte por depósitos no Banco da Inglaterra. Eles são intercambiáveis com outras notas de libra em uma base de um para um, e circulam livremente no Reino Unido, embora não na Escócia e na Irlanda do Norte. Na verdade, tecnicamente, nenhuma nota de banco (incluindo notas do Banco da Inglaterra) se qualifica para curso legal na Escócia ou na Irlanda do Norte.[5]

Moedas complementares

Cédula de 5 Libras de Bristol.

Cédulas de 1, 5 e 10 Dólares de Calgary.

Cédula de 5 Palmas, moeda social emitida pelo Banco Palmas, banco comunitário da cidade de Fortaleza.
Muitas moedas privadas são comunitárias ou sociais, tipos de moeda complementar.

Desde 1991, residentes de e da cidade de Ithaca, oeste do estado de Nova York, utilizaram uma moeda privada em que os trabalhadores participantes ganham ou compram Ithaca Hours, que podem ser usados para comprar bens e serviços localmente. O sistema foi considerado legal desde que todas as receitas derivadas das Hours sejam reportadas ao IRS (Internal Revenue Service) como renda.

BerkShares é uma moeda local que circula na região de Berkshires em Massachusetts. Foi lançado em 29 de Setembro de 2006 pela BerkShares Inc., com assistência de pesquisa e desenvolvimento da E. F. Schumacher Society (New Economy Coalition). O site BerkShares lista mais de 370 empresas no condado de Berkshire que aceitam a moeda. Em 30 meses, 2.2 milhões de BerkShares foram emitidos a partir de 12 filiais de cinco bancos locais.

A Inglaterra teve a Libra de Totnes que foi lançada; pelo movimento cidades em Transição, pela Totnes Economics e Livelihoods group, em Março de 2007. A Libra de Totnes é igual a uma libra esterlina e é lastreada pela libra esterlina numa conta bancária. Em Setembro de 2008, cerca de 70 empresas na cidade de Totnes estavam aceitando a Libra de Totnes. Outras moedas locais lançadas desde então incluem a Libra de Lewes (2008), a Libra de Brixton (2009),[6] a Libra de Stroud (2009) [7] e a Libra de Bristol, que também permite pagamentos eletrônicos.[8]

A Áustria teve o experimento de Wörgl de Julho de 1932 a Setembro de 1933.

A Baviera na Alemanha, tem o Chiemgauer desde 2003. A partir de 2011 haviam mais de 550 mil em circulação.[8]

Desde o início de 2006, a "Iniciativa da Cidade de Karlsruhe" emitiu o Karlsruher, que não tem valor nominal. Cada moeda tem o valor de 50 Eurocents e é usado principalmente em garagens de estacionamento. A partir de 2009, 120 empresas em Karlsruhe aceitam o Karlsruher e geralmente concedem um desconto em pagamentos efetuados com ele.[9]

No Canadá, inúmeras moedas complementares estão em uso, como dólar de Calgary e dólar de Toronto. No entanto, as moedas privadas no Canadá não podem ser referidas como sendo legais e muitas moedas privadas (bem como programas de fidelidade) evitam a palavra "dólar", usando nomes como "cupons" ou "dólares", para evitar confusão. Exemplos incluem: Canadian Tire money e Pioneer Energy's Bonus Bucks.

Os programas de fidelidade e recompensa ao cliente, operados por empresas, às vezes são contados como moedas privadas. No entanto, embora "pontos" ou "programas de milhagem" possam ser trocados por mercadorias ou viagens do patrocinador do programa, a maioria deles não tem o elemento-chave para que a "moeda" seja um meio de troca transferível para outras pessoas e utilizável como pagamento de itens de outros fornecedores . Alguns programas têm "parcerias" que permitem isso até certo ponto, e permitem a transferência de pontos ou milhas. Algumas startups, como o site canadense Points.com,[10] procuram tornar a fidelidade por "pontos" mais monetária, criando uma troca em que os pontos de um programa de fidelidade podem ser negociados por pontos em outros programas.[11]

Criptografia e moedas digitais

Vera valor:[12] exemplo de DGC (digital gold currency.
A criptomoeda é uma forma de moeda virtual ou digital onde a criptografia assegura as transações e controla a criação de unidades adicionais da moeda.[13] Os sistemas criptográficos utilizados permitem descentralização; uma criptografia descentralizada é moeda fiduciária, sem um sistema bancário central. A criptomoeda já foi apontada como responsável pelo ressurgimento do sistema bancário livre.[14]Em termos de valor de mercado absoluto, o Bitcoin é a criptomoeda mais valiosa,[15] mas existem mais de 1.000 moedas digitais.[16]

Em 6 de Agosto de 2013, nos EUA, o juiz federal Amos Mazzant do distrito oriental do quinto circuito do Texas, determinou que os Bitcoins são "uma moeda ou uma forma de dinheiro" (especificamente os valores mobiliários, conforme definido pelas Leis de Títulos Federais) e, como tal, estavam sujeitos à jurisdição do tribunal.[17] [18] Em Agosto de 2013, o ministério das finanças alemão caracterizou a Bitcoin como uma unidade de conta,[19] [20] utilizável em círculos multilaterais de compensação financeira e sujeita ao imposto sobre os ganhos de capital, se realizada em menos de um ano.[19] [20]

Na Tailândia, a falta de legislação leva muitos a acreditar que o Bitcoin está proibido.[21]

Crimes de divisas privadas
À medida que moedas nacionais podem ser falsificadas, o mesmo ocorre com as moedas privadas que estão sujeitas a outras questões criminais, incluindo a fraude.

O Liberty Dollar era uma moeda privada com base em commodities criada por Bernard von NotHaus e emitida entre 1998 e 2009. Em 2011, von NotHaus foi preso e posteriormente condenado por acusações de lavagem de dinheiro, fraude postal, fraude eletrônica, contrafação e conspiração.[22] [23] As cobranças deram origem à visão do governo de que as moedas de prata Liberty também se assemelhavam à cunhagem oficial.

Em 2007, Angel Cruz, fundador da The United Cities Corporation (TUC), anunciou a criação uma moeda alternativa baseada em ativos denominada "United States Private Dollars".[24] Cruz afirmou que os United States Private Dollars estavam "apoiados pelo patrimônio líquido total dos ativos de seus membros" e imprimiram 6 bilhões de dólares da moeda privada.[25] Os ativos de apoio foram estimados em 357 bilhões de dólares.[26] A moeda mostrou o slogan "Em Jeová, nós confiamos".[27] O Office of the Comptroller of the Currency emitiu um alerta que os cheques emitidos pelo TUC eram "instrumentos sem valor" e não deveriam ser cobrados.[27] Em 2008, Cruz foi indiciado por um grande júri federal na Flórida sob acusação de conspiração para defraudar os Estados Unidos nos termos do Artigo 18 Inciso 1344 e Artigo 18 Inciso 371 e seis acusações de fraude bancária de acordo com o Inciso 2 do mesmo Artigo, do código dos Estados Unidos em relação com seus negócios com o Bank of America, enquanto tentava obter os cédulas bancárias da cobradas pela United Cities.[28] No final de Outubro de 2010, Cruz ainda estava foragido,[29] embora um associado tenha sido condenado por acusações relacionadas e condenado à oito anos de prisão.

Ver também
Criação de moeda
Dinheiro circulante
Dinheiro de emergência
Moeda-mercadoria
Notgeld
Padrão-ouro
Senhoriagem
Sistema bancário sombra
Teoria quantitativa da moeda
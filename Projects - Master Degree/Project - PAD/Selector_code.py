import sys
import pandas as pd
import numpy as np
import datetime
import math as m

from collections import Counter

inici=datetime.datetime.now

def scp_aux(firstString, secondString , counterClass):
    return counterClass[firstString]*counterClass[secondString]

def dice_aux(firstString, secondString,counterClass):
    return counterClass[firstString]+counterClass[secondString]

def scp_and_dice (keyword,counterClass):
    #Neste if ignoramos todas a keywords que cotenham pontuaÃ§Ã£o    
    keywordSplit = keyword.split()
    keywordSize=len(keywordSplit)
    firstString=keywordSplit.pop(0)
    x=scp_aux(firstString, " ".join(keywordSplit),counterClass)
    y=dice_aux(firstString, " ".join(keywordSplit),counterClass)
    for n in range(1, keywordSize):
        firstString+= " "+keywordSplit.pop(0)
        x+=scp_aux(firstString, " ".join(keywordSplit),counterClass)
        y+=dice_aux(firstString, " ".join(keywordSplit),counterClass)

    x_y = counterClass[keyword]
    #metrica(calculos)
    Cohesion_SCP = (x_y**2) / ((1/(keywordSize-1))*x)
    Cohesion_Dice = (x_y*2) / ((1/(keywordSize-1))*y)
    result=[Cohesion_SCP, Cohesion_Dice, x_y]
    return result

def probabilityDot(A):
    probality=0
    for doc in textDocuments:
        probality+=doc[1][A]/doc[2]
    return 1/len(textDocuments)*probality

def covariance(A,B):
    probabilityDotA=probabilityDot(A)
    probabilityDotB=probabilityDot(B)
    sumP=0
    for doc in textDocuments:
        sumP+=((doc[1][A]/doc[2])-probabilityDotA)*((doc[1][B]/doc[2])-probabilityDotB)
    return (1/(len(textDocuments)-1))*sumP

def correlation(A,B):
    return covariance(A, B)/(m.sqrt(covariance(A,A))*m.sqrt(covariance(B,B)))



def calculateHitsInDocs(expressao):
    counterHits=0
    for i in  range(0, len(files)):
        if textDocuments[i][1].__contains__(expressao) :
            counterHits=counterHits+1
    return counterHits


def calculateTf_idf(expressao,counterClass, nPalavras):

    freqW = counterClass[expressao]
    hitsInDocs = calculateHitsInDocs(expressao)
    
    return  ((freqW/nPalavras) * np.log(len(files)/hitsInDocs))

def nearest(a, aSize, b, bSize):
    near=sys.maxsize
    for i in a:
        for j in b:
            test=i-j
            if(test>0):
                test-=bSize
                test=abs(test)
            elif(test<0):
                test+=aSize
                test=abs(test)
            if(test<near):
                near=test
    return near

def farthst(a,aSize, b, bSize):
    farth=0
    for i in a:
        for j in b:
            test=i-j
            if(test>0):
                test-=bSize
                test=abs(test)
            elif(test<0):
                test+=aSize
                test=abs(test)
            if(test>farth):
                farth=test
    return farth
#Receber as duas expressoes numa lista
def Ipalg(a, b):
    print(a)
    print(b)
    comunDocs=0
    index_a=[]
    index_b=[]
    IPSum=0
    if a!=b:
        for doc in textDocuments:
            comunDocs+=1
            print(doc[1][a])
            print(doc[1][b])
            if doc[1][a]!=0 and doc[1][b]!=0:
                a_array=a.split(" ")
                b_array=b.split(" ")
                word=doc[4]
                for i in range(0,len(doc[4])-1):
                    c=0
                    if word[i]==a_array[0]:
                        while c<len(a_array)-1 and word[i+c]==a_array[c]:
                            c+=1
                        if " ".join(word[i:i+c+1])==a:
                            index_a.append(i)
                    if word[i]==b_array[0]:
                        c=0
                        while c<len(b_array)-1 and word[i+c]==b_array[c]:
                            c+=1
                        if " ".join(word[i:i+c+1])==b:
                            index_b.append(i)
                    i+=c
                if(farthst(index_a, len(a_array), index_b, len(b_array))==0):
                    IPSum+=1
                else:
                    IPSum+=nearest(index_a, len(a_array), index_b, len(b_array))/farthst(index_a, len(a_array), index_b, len(b_array))

        if comunDocs>0:
            return 1-(1/comunDocs)*IPSum
        else:
            return 0
    else:
        return 1


#Lista dos nomes dos docs a serem usados
files=['1st','2st','3st','4st','6st','7st','8st','9st','10st','11st','12st','13st','14st','15st','16st','17st','18st','19st','20st','21st','22st']
#Matriz dos documentos com : o texto dos docs [_,0],  o counter de todas as palavras do texto [_,1],  o numero de palavras do doc  [_,2] e com o nome do documento [_,3]
textDocuments = [[0 for x in range(5)] for y in range(len(files))] 
#-----------------------------------Tratamento do set de documentos-----------------------------------# 
print('#############################################')
print('Inicio de processamento do set de documentos')
    
for nDoc in  range(0, len(files)):
    print('Processamento do Doc',nDoc+1,':',files[nDoc])
    fileName = files[nDoc]
    document = open(fileName+'.txt', 'r', encoding='utf-8') 
    textDoc = document.read()
    
    #Posicao com o texto
    textDocuments[nDoc][0]=textDoc

    #limpar os tokens irrelevantes
        #sinais de pontuaÃ§Ã£o
    textDoc = textDoc.replace('.',' ')
    textDoc = textDoc.replace(',',' ')
    textDoc = textDoc.replace('?',' ')
    textDoc = textDoc.replace('!',' ')
    textDoc = textDoc.replace(':',' ')
    textDoc = textDoc.replace(';',' ')
        #chavetas, parÃªntese, etc....
    textDoc = textDoc.replace('<',' ')
    textDoc = textDoc.replace('>',' ')
    textDoc = textDoc.replace('(',' ')
    textDoc = textDoc.replace(')',' ')
    textDoc = textDoc.replace('[',' ')
    textDoc = textDoc.replace(']',' ')
    textDoc = textDoc.replace('{',' ')
    textDoc = textDoc.replace('}',' ')
    textDoc = textDoc.replace('|',' ')
    textDoc = textDoc.replace('/',' ')
    textDoc = textDoc.replace('=',' ')
    
        #sinais
    textDoc = textDoc.replace('*',' ')    
    # textDoc = textDoc.replace('-',' ')
    textDoc = textDoc.replace('+',' ')    
    textDoc = textDoc.replace('@',' ')
    textDoc = textDoc.replace('Â»',' ')
    textDoc = textDoc.replace('Â«',' ')    
    textDoc = textDoc.replace('"',' ')
    textDoc = textDoc.replace('\n',' ')
    textDoc = textDoc.replace('  ',' ')
    
    #separar as palavras por espaÃ§os
    listaPalavras = textDoc.split()
    #Counter das palavras do texto
    textDocCounter = Counter(listaPalavras)
    
    
    #PosiÃ§Ã£o com a class counter de todas as palavras do texto
    textDocuments[nDoc][1]=textDocCounter
    #PosiÃ§Ã£o com o numero de palavras total do documento
    textDocuments[nDoc][2]=len(listaPalavras)
#   PosiÃ§Ã£o com o nome do doc
    textDocuments[nDoc][3]=fileName
    #Posicao com lista de palavras
    textDocuments[nDoc][4]=listaPalavras

print('Fim do processamento dos documentos')
print('#############################################')
#-----------------------------------------------------------------------------------------------------# 



#----------------------------------Calculos de cada 1 dos documentos----------------------------------# 

print('Inicio da analise de cada documento')
doc=datetime.datetime.now
sinais=set([".",":",",",";","-","_","/","<",">","|","\"","'","(",")","{","}","[","]","Â«","Â»","=","?","!","#at#","+","@","*"])

for nDoc in  range(0, len(files)):
    print('Analise do Doc',nDoc+1,':',textDocuments[nDoc][3])
    

    conjuntos=[]
    Cohesions_SCPs=[]
    Cohesions_Dices=[]
    Cohesions_AVG=[]
    Feq=[]    
    tf_IDFs=[]
    tf_IDFn=[]

    palavras1=[]
    palavras2=[]
    conjuntosTemp=[]
    text=textDocuments[nDoc][0]

    text = text.replace('.',' . ')
    text = text.replace(',',' , ')
    text = text.replace('?',' ? ')
    text = text.replace('!',' ! ')
    text = text.replace(':',' : ')
    text = text.replace(';',' ; ')
        #chavetas, parêntese, etc....
    text = text.replace('<',' < ')
    text = text.replace('>',' > ')
    text = text.replace('(',' ( ')
    text = text.replace(')',' ) ')
    text = text.replace('[',' [ ')
    text = text.replace(']',' ] ')
    text = text.replace('{',' { ')
    text = text.replace('}',' } ')
    text = text.replace('|',' | ')
    text = text.replace('/',' / ')
    text = text.replace('=', ' = ')    
        #sinais
    text = text.replace('*',' * ')    
    # textDoc = textDoc.replace('-',' ')
    text = text.replace('+',' + ')    
    text = text.replace('@',' @ ')
    text = text.replace('Â»',' Â» ')
    text = text.replace('Â«',' Â« ')
    text = text.replace('"'," ' " )
    text = text.replace('\n',' #at# ')
    text = text.replace('  ',' ')

    listaPalavras = text.split() #NOTA PODEMOS GUARDAR ISTO NO VECTOR PARA NAO SER NECESSARIO FAZER ISTO AGAIN (DUVIDA, NECESSARIO TEXTO???)
    
    #------------------- CrianÃ§Ã£o de n-grams(2-8) para cada uma das palavras do texto---------------------# 
    
    for i in range(0, len(listaPalavras)-1):
        
        conjuntosTemp.append(listaPalavras[i] + ' ' + listaPalavras[i+1])    
        if i==0:
            palavras1.append(None)
        else:
            palavras1.append(listaPalavras[i-1])
        if i>len(listaPalavras)-3:
            palavras2.append(None)
        else:
            palavras2.append(listaPalavras[i+2])
        for a in range(2,8):
            if a+i<len(listaPalavras):
                conjuntosTemp.append(conjuntosTemp[-1] + ' ' + listaPalavras[i+a])
                if i==0:
                    palavras1.append(None)
                else:
                    palavras1.append(listaPalavras[i-1])
                if a+i+1<=len(listaPalavras):
                    palavras2.append(None)
                else:
                    palavras2.append(listaPalavras[a+i+1])
    #Update da class counter do documento com os n-gramas
    textDocuments[nDoc][1] += Counter(conjuntosTemp)
    #-----------------------------------------------------------------------------------------------------# 

    
       
    blackList=set()
    fail=False
    
    counterClass = textDocuments[nDoc][1]
    nPalavras = textDocuments[nDoc][2]

    for i in range(0, len(conjuntosTemp)):
        keyword = conjuntosTemp[i]
        #Variavel auxiliar usada para passar o keyword para um set para verificar se existe sinais de pontuaÃ§Ã£o nesta keyword
        TempKeyword = set(keyword.split())
        #Neste if ignoramos todas a keywords que cotenham pontuaÃ§Ã£o
        if len(TempKeyword.intersection(sinais))== 0 :    
            keywordSplit = keyword.split()
            if(counterClass[keyword]!=1 ):
                if(keyword not in blackList):
                    if(keyword=="Ludwig von"):
                        print("alerta")
                    this_result=scp_and_dice(keyword, counterClass)
                    this_SCP=this_result[0]
                    fail=False
                    if palavras1[i] is not None and palavras1[i] not in sinais:
                        if not(this_SCP>scp_and_dice(palavras1[i]+ " " +keyword,counterClass)[0]):
                            blackList.add(keyword)
                            fail=True
                    if palavras2[i] is not None and palavras2[i] not in sinais:
                        if not fail and not this_SCP>scp_and_dice(keyword + " " +palavras2[i], counterClass)[0] :
                            blackList.add(keyword)
                            fail=True
                    if not fail and len(keywordSplit)>2 and (this_SCP<scp_and_dice(" ".join(keywordSplit[1:]),counterClass)[0]) :
                        blackList.add(keyword)
                        fail=True
                    if not fail and len(keywordSplit)>2 and (this_SCP<scp_and_dice(" ".join(keywordSplit[:-1]),counterClass)[0]) :
                        blackList.add(keyword)
                        fail=True
                    if not fail and keyword not in conjuntos:
                        conjuntos.append(keyword)
                        Cohesions_SCPs.append(this_SCP) 
                        Cohesions_Dices.append(this_result[1]) 
                        Feq.append(this_result[2])
                        #tf_IDFs.append(calculateTf_idf(keyword,nDoc))
                    elif fail and keyword in conjuntos:
                        index = conjuntos.index(keyword)
                        del conjuntos[index]
                        del Cohesions_SCPs[index]
                        del Cohesions_Dices[index]
                        del Feq[index]
                        #del tf_IDFs[index]
    
    print("Ver tf_idf")
    for i in range(0, len(conjuntos)):
        tf_idf=calculateTf_idf(conjuntos[i],counterClass, nPalavras)
        tf_IDFs.append(tf_idf)
        tf_IDFn.append(tf_idf*len(conjuntos[i].split()))

    print('Fim da analise do doc:', textDocuments[nDoc][3])
    #------------------- Guardar os resultados num doc diferente de cvs para cada documento analisado---------------------# 
    print('Inicio de armazenamento dos resultados do doc:', textDocuments[nDoc][3])    
    
    resultados_temp = {'Conjuntos':conjuntos, 'Frequence':Feq , 'Cohesion_SCP':Cohesions_SCPs , 'Cohesion_Dice':Cohesions_Dices, 'Tf_Idf':tf_IDFs, 'Tf_Idf vezes n':tf_IDFn}
    resultados = pd.DataFrame(resultados_temp).sort_values(by=['Tf_Idf vezes n'], ascending=False) 
    #csv tem no inicio o nome do ficheiro de texto
    resultados.to_csv(textDocuments[nDoc][3]+'_Resultados.csv')
    
    print('Resultados armazenados')
    print('\n')

#Fazer Score
for doc in textDocuments:
    name=doc[3]
    REs=pd.read_csv(name+'_Resultados.csv', sep=",")
    tops=REs.head(n=10)
    scores=[]
    for expression in REs.iterrows():
        score=0
        for top in tops.iterrows():
            a=correlation(expression[1]['Conjuntos'], top[1]['Conjuntos'])
            b=m.sqrt(Ipalg(expression[1]['Conjuntos'], top[1]['Conjuntos']))
            score+=(a*b)/(top[0]+1)
        print(expression[1]['Conjuntos'])
        scores.append(score)
    REs['Score']=scores
    REs.to_csv(name+'_Resultados.csv')
    print(len(scores))
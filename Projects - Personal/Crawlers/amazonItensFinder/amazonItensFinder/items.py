# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field

class Amazonitensfinder_Item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    priceDollar = Field()
    priceEuro = Field()
    name = Field()
    link = Field()
    rating_Amazon = Field()
    rating_Martins = Field()
    n_votes = Field()
    category = Field()
    amazonId_ASIN = Field()
# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exporters import CsvItemExporter
import datetime

exchange_rate = 0.811956
#domino principal do site amazon
amazonSite ='https://www.amazon.com'

class AmazonItensfinded_Pipeline(object):
    
    # metodo chamado no inicio da pipeline
     def __init__(self):
         now = str(datetime.date.today())
         self.file = open("Amazon_"+now+".csv", 'wb')
         self.exporter = CsvItemExporter(self.file, True)
         self.exporter.fields_to_export=['name','link','n_votes','rating_Amazon','rating_Martins','priceDollar','priceEuro','category','amazonId_ASIN']
         self.exporter.start_exporting()

    # metodo chamado no fim da pipeline      
     def close_spider(self, spider):      
         self.exporter.finish_exporting()
         self.file.close()

   

    #metodo que vai processar todos os itens extraidos (limpar/criar novos dados com os dados extraidos/adaptar os dados ao que quero)
     def process_item(self, item, spider):    
        
        if item['priceDollar']:
            # limpar o preco recebido (tirar as , e o $) 
            # converter o preço (dolar) para(euro) com a variavel global de conversao
            item['priceDollar'] = item['priceDollar'].replace('$','')
            item['priceDollar'] = float(item['priceDollar'].replace(',',''))
            item['priceEuro'] = float(item['priceDollar']) * exchange_rate
            item['priceEuro'] = float(format(item['priceEuro'],'.2f'))

        if item['name']:
            #limpar o nome recebido
            #"\n            Fire 7 Tablet with Alexa, 7\" Display, 8 GB, Black - with Special Offers\n        "
            # remover ao inico : "\n            "
            #remover ao fim    : "\n        "
            item['name']= item['name'][13:]
            item['name']= item['name'][:-9]

        if item['link']:
            # /Magicstick-Powerful-Featured-Windows10-Preloaded/dp/B078WJSHH2?_encoding=UTF8&psc=1
            #  adicionar ao inicio do link:
            # https://www.amazon.com
            item['link']= amazonSite + (item['link'])

        if item['rating_Amazon']:
            #limpar o rating recebido
            #5.0 out of 5 stars
            #   |^limpar apartir daqui
            item['rating_Amazon']=float(item['rating_Amazon'][:-15])

        if item['n_votes']:  
            #limpar os votos (virgula)
            #3,207
           item['n_votes'] = int(item['n_votes'].replace(',',''))
        
        if item['amazonId_ASIN'] is not 'not-found':
            #ref":"zg_bs_automotive_98","asin":"B015G7RVTY"
            #É necessário na variavel 1 dividir a string por " para na variavel 2 ir á lista de 1 buscar o codigo asin [penultima posicao]
            temp_asin1 = item['amazonId_ASIN'].split('"')
            temp_asin2 = temp_asin1[-2]
            item['amazonId_ASIN']= temp_asin2
       
        item['rating_Martins'] = AmazonItensfinded_Pipeline.calcular_Rating(int(item['n_votes']),float(item['rating_Amazon']))
        if item['rating_Martins'] < 0:
            item['rating_Martins']=0.0
        
        
        self.exporter.export_item(item)
        return item

        
        
     #metodo para dar mais valor ao rating com mais votos e panalizar os com poucos   
     #ta feito a pedreiro, necessário estar num estado de menos pedra para ir ver formulas/estatisticas
     def calcular_Rating(numero_votos,rating):
            
            margem1 = 50
            margem2 = 200
            margem3 = 500
            margem4 = 1000
            
            corte1 = 2.0
            corte2 = 0.6
            corte3 = 0.3
            corte4 = 0.2
            
            if numero_votos <= margem1:
                return float(format(rating-corte1,'.2f'))
            if numero_votos <= margem2:
                return float(format(rating-corte2,'.2f'))
            if numero_votos <= margem3:
                return float(format(rating-corte3,'.2f'))
            if numero_votos <= margem4:
                return float(format(rating-corte4,'.2f'))
            return rating
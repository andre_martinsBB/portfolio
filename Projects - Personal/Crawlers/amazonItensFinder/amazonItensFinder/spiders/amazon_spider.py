# -*- coding: utf-8 -*-
"""
Created on Wed Mar  7 14:30:05 2018

@author: Andre-X
"""
import scrapy
from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector

from amazonItensFinder.items import Amazonitensfinder_Item 

class AmazonSpider(scrapy.Spider):
    name = "amazon"

#==============================================================================
#   Exemplo de usar argumentos(TAG)
#       Existe um url predefenido. Quando corremos o codigo podemos passar um 
#         argumento para ir a um sitio especifico do site(url) predefenido
#  
#         Ex: Vamos dar crawl na pagina :"http://quotes.toscrape.com/" + "tag"
# 
#     def start_requests(self):
#         url = 'http://quotes.toscrape.com/'
#         tag = getattr(self, 'tag', None)
#         if tag is not None:
#             url = url + 'tag/' + tag
#         yield scrapy.Request(url, self.parse)
#==============================================================================


    def start_requests(self):
#==============================================================================
#        Objects from URLs, you can just define a start_urls class
#        attribute with a list of URLs. 
#        This list will then be used by the default implementation 
#        of start_requests() to create the initial requests for your spider
#==============================================================================
        urls = [            
        #BEST SELLERS
            'https://www.amazon.com/Best-Sellers/zgbs/ref=zg_bsnr_tab',
         #NEW RELEASES    
            'https://www.amazon.com/gp/new-releases/ref=zg_mw_tab'
        #MOST WISHED FOR
            'https://www.amazon.com/gp/most-wished-for/ref=zg_bs_tab',
        #GIFT IDEAS
            'https://www.amazon.com/gp/most-gifted/ref=zg_bsnr_tab' 
        ]
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse1_categorias)

            
            
            
    def parse1_categorias(self,response):
       #Nota: ver selectors na documentação do scrapy
       hxs = scrapy.Selector(response)
       #lista das varias categorias
       categorias = hxs.xpath('//*[(@id = "zg_browseRoot")]//ul//li')  
       print('CCCCCCCCategorias')
       print(categorias)
       for categoria in categorias:
           next_url = categoria.xpath('a/@href').extract_first(default='not-found')
           yield response.follow(next_url, self.parse)
       
    def parse(self, response):
#==============================================================================
#         The parse() method will be called to handle each of the requests 
#         for those URLs, even though we haven’t 
#         explicitly told Scrapy to do so. 
#         This happens because parse() is Scrapy’s default callback method,
#         which is called for requests without an explicitly assigned callbac      
#==============================================================================

       #Nota: ver selectors na documentação do scrapy
       hxs = scrapy.Selector(response)
       #lista dos produtos a extrair
       produtos = hxs.xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "zg_itemImmersion", " " ))]')
       #Extrair as infos dos vários produtos (1 página)
       for produto in produtos:
           #Iten defenido no ficheiro itens do projecto
           item = Amazonitensfinder_Item()   
           #Preco
           item['priceDollar'] = produto.css('span.p13n-sc-price::text').extract_first(default=0)
           #Nome                     ]
           item['name'] = produto.xpath('div[2]/div/a/div[2]/text()').extract_first(default='not-found') 
           #Link
           item['link'] = produto.css('a.a-link-normal::attr(href)').extract_first(default='not-found')     
           #Rating
           item['rating_Amazon'] = produto.css('a.a-link-normal::attr(title)').extract_first(default=0)         
           #Nº de Votos
           item['n_votes'] = produto.css('a.a-size-small::text').extract_first(default=0)
           #Amazon product ID
           item['amazonId_ASIN'] = produto.xpath('div[2]/div/@data-p13n-asin-metadata').extract_first(default='not-found')  
           #Categoria
           #como este parametro não está na mesma directoria dos outros objectos tive de criar um novo xpath (para ir para outro local)
           categoria = hxs.xpath('//*[(@id = "zg_browseRoot")]//*[contains(concat( " ", @class, " " ), concat( " ", "zg_selected", " " ))]')
           item['category'] = categoria.css('span.zg_selected::text').extract_first(default='not-found')

           yield item
     
    #Seguimento de páginas
       #Visto a paginação ser por barra (todas a opcões na barra e a conrrente pagina marcada) tive que fazer desta maneira
       pages = hxs.xpath('//*[(@id = "zg_paginationWrapper")]//li')
       #flag para ajudar a encontrar a proxima pagina a dar crwal
       flag= False
       #Para todas as pagina na barra de paginação
       for page in pages :
           current_page = page.xpath('@class').extract_first(default='not-found') 
           if flag == True :
               #obter o ulr da proxima pagina, e seguir para a mesma
               next_url = page.xpath('a/@href').extract_first(default='not-found')
               yield response.follow(next_url, self.parse)
               break
           if current_page == 'zg_page zg_selected' :
               #Ao estar na pagina selecionado, quer dizer que a proxima pagina da lista é a que queremos dar crawl
               flag= True 
              
      
    

# -*- coding: utf-8 -*-
"""
Created on Sun Apr  8 15:54:44 2018

@author: Andre-X
"""

import scrapy
from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector

from FreeProxysFinder.items import Freeproxysfinder_Item 


#==============================================================================
# Esta spider faz crawl ao seguinte site :
# https://free-proxy-list.net/
#==============================================================================


class Free_ProxList_Spider(scrapy.Spider):
  
    name = "free_proxy_list"

#==============================================================================
#   Exemplo de usar argumentos(TAG)
#       Existe um url predefenido. Quando corremos o codigo podemos passar um 
#         argumento para ir a um sitio especifico do site(url) predefenido
#  
#         Ex: Vamos dar crawl na pagina :"http://quotes.toscrape.com/" + "tag"
# 
#     def start_requests(self):
#         url = 'http://quotes.toscrape.com/'
#         tag = getattr(self, 'tag', None)
#         if tag is not None:
#             url = url + 'tag/' + tag
#         yield scrapy.Request(url, self.parse)
#==============================================================================


    def start_requests(self):
#==============================================================================
#        Objects from URLs, you can just define a start_urls class
#        attribute with a list of URLs. 
#        This list will then be used by the default implementation 
#        of start_requests() to create the initial requests for your spider
#==============================================================================
        urls = [            
            'https://free-proxy-list.net/' 
        ]
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

       
    def parse(self, response):
#==============================================================================
#         The parse() method will be called to handle each of the requests 
#         for those URLs, even though we havenâ€™t 
#         explicitly told Scrapy to do so. 
#         This happens because parse() is Scrapyâ€™s default callback method,
#         which is called for requests without an explicitly assigned callbac      
#==============================================================================

       hxs = scrapy.Selector(response)
       proxies = hxs.xpath('//*[@id="proxylisttable"]/tbody/tr')

       #Extrair as infos dos varios proxys (vai buscar as 300 entradas)
       for proxy in proxies:           
         # Só se vai buscar os elite proxies ( são os unicos que garatem 100% das vezes que o ip real é escondido no request)  
         if proxy.xpath('td[5]/text()').extract_first(default='not-found') == 'elite proxy' :
           
           #Iten defenido no ficheiro itens do projecto 
           item = Freeproxysfinder_Item()   
           
           item['ipAddress'] = proxy.xpath('td[1]/text()').extract_first(default='not-found') 
           
           item['port'] = proxy.xpath('td[2]/text()').extract_first(default='not-found')

           item['country'] = proxy.xpath('td[4]/text()').extract_first(default='not-found')

           item['anonymity'] = proxy.xpath('td[5]/text()').extract_first(default='not-found')

           item['protocol'] = proxy.xpath('td[7]/text()').extract_first(default='not-found')
           
           if item['protocol'] == 'no' :
               item['protocol']= 'HTTP'
           if item['protocol'] == 'yes' :
               item['protocol']= 'HTTPS'
               
           yield item

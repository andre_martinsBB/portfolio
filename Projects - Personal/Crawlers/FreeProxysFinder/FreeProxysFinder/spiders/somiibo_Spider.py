# -*- coding: utf-8 -*-
"""
Created on Sun Apr  8 15:54:44 2018

@author: Andre-X
"""

import scrapy
from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector

from FreeProxysFinder.items import Freeproxysfinder_Item 


#==============================================================================
# Esta spider faz crawl ao seguinte site :
# https://free-proxy-list.net/
#==============================================================================


class Somiibo_Spider(scrapy.Spider):
  
    name = "somiibo"

#==============================================================================
#   Exemplo de usar argumentos(TAG)
#       Existe um url predefenido. Quando corremos o codigo podemos passar um 
#         argumento para ir a um sitio especifico do site(url) predefenido
#  
#         Ex: Vamos dar crawl na pagina :"http://quotes.toscrape.com/" + "tag"
# 
#     def start_requests(self):
#         url = 'http://quotes.toscrape.com/'
#         tag = getattr(self, 'tag', None)
#         if tag is not None:
#             url = url + 'tag/' + tag
#         yield scrapy.Request(url, self.parse)
#==============================================================================


    def start_requests(self):
#==============================================================================
#        Objects from URLs, you can just define a start_urls class
#        attribute with a list of URLs. 
#        This list will then be used by the default implementation 
#        of start_requests() to create the initial requests for your spider
#==============================================================================
       
        #Site original : https://somiibo.com/tools/proxy-list/
        #URL do google docs usado neste site
         urls = [            
            'https://docs.google.com/spreadsheets/d/e/2PACX-1vSZsveUIUla7ugYOWXNe1M5YYzXARGMcsf_Ax3h8eg-hoeWi9bSuqgRPZzNitt3zDSjdb9HgH_Y1n1Y/pubhtml?gid=1969601033&single=true&widget=false&headers=false&rm=minimal&range=a1:j1000' 
        ]
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

       
    def parse(self, response):
#==============================================================================
#         The parse() method will be called to handle each of the requests 
#         for those URLs, even though we havenâ€™t 
#         explicitly told Scrapy to do so. 
#         This happens because parse() is Scrapyâ€™s default callback method,
#         which is called for requests without an explicitly assigned callbac      
#==============================================================================

       hxs = scrapy.Selector(response)
       proxies = hxs.xpath('//*[@id="1969601033"]/div/table/tbody/tr')

       
  
       #Extrair as infos dos varios proxys
       for proxy in proxies:      
         #Iten defenido no ficheiro itens do projecto 
         item = Freeproxysfinder_Item()     
           
        #So vai buscar as entradas da tabela
         if proxy.xpath('@style').extract_first(default='not-found') == 'height:30px;' :
            # Só se vai buscar os high level proxies ( são os unicos que garatem 100% das vezes que o ip real é escondido no request
            if proxy.xpath('td[5]/text()').extract_first(default='not-found') == 'high':
                # print('ID')
                # print(proxy.xpath('td[1]/text()').extract_first(default='not-found'))
                 
                #IP
                if proxy.xpath('td[2]/@class').extract_first(default='not-found') == 's6 softmerge':
                    item['ipAddress'] = proxy.xpath('td[2]/div/text()').extract_first(default='not-found')        
                if proxy.xpath('td[2]/@class').extract_first(default='not-found') == 's7':
                    item['ipAddress'] = proxy.xpath('td[2]/text()').extract_first(default='not-found')
                    
                #Porta
                item['port'] = proxy.xpath('td[3]/text()').extract_first(default='not-found')
                #Anonimidade 
                item['anonymity'] = proxy.xpath('td[5]/text()').extract_first(default='not-found') 
                #Protocolo
                item['protocol'] = proxy.xpath('td[4]/text()').extract_first(default='not-found')
                #Contry
                item['country'] = proxy.xpath('td[6]/text()').extract_first(default='not-found')
                
                yield item



# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field

class Freeproxysfinder_Item(scrapy.Item):
    ipAddress = Field()
    port = Field()
    country = Field()
    anonymity = Field()
    protocol = Field()

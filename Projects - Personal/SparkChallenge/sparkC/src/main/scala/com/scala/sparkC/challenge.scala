package com.scala.sparkC

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.hadoop.fs._;

object challenge {

  def main(args: Array[String]) {

    //To set HADOOP_HOME.
    //System.setProperty(“hadoop.home.dir”, “c://hadoop//”);

    // Create a SparkSession. No need to create SparkContext
    // You automatically get it as part of the SparkSession
    val spark = SparkSession
      .builder()
      .master("local")
      .appName("SparkChallenge")
      .getOrCreate()

    //Enable fileSytem usage
    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration);

    val dfApps = spark.read
      .options(Map("inferSchema" -> "true", "delimiter" -> ",", "header" -> "true"))
      .csv("datasets/googleplaystore.csv")
      .distinct()

    val dfUserRev = spark.read
      .options(Map("inferSchema" -> "true", "header" -> "true"))
      .csv("datasets/googleplaystore_user_reviews.csv")
      .na.replace(Seq("Sentiment_Polarity"), Map("nan" -> "0.0"))

    ////____________________EX 1____________________

    // Register the DataFrame as a SQL temporary view
    dfUserRev.createOrReplaceTempView("revs")

    val df1 = spark.sql("SELECT App, AVG (Sentiment_Polarity) AS Average_Sentiment_Polarity FROM revs GROUP BY App")
    // df1.show()

    ////____________________EX 2____________________

    // Register the DataFrame as a SQL temporary view
    dfApps.createOrReplaceTempView("apps")

    val dfBestApps = spark.sql("SELECT App, Rating FROM apps WHERE Rating >= 4 SORT BY Rating DESC ")

    //Save df to Csv
    dfBestApps.coalesce(1)
      .write.mode("overwrite")
      .format("csv")
      .option("delimiter", "§")
      .option("header", "true")
      .save("results/Ex_2")
    //Find the created file in FS
    val fileCsv = fs.globStatus(new Path("results/Ex_2/part*"))(0).getPath().getName();
    //Rename the file
    fs.rename(new Path("results/Ex_2/" + fileCsv), new Path("results/Ex_2/best_apps.csv"));

    ////____________________EX 3____________________

    //Rename Colums
    val df2tempRename = dfApps.withColumnRenamed("Category", "Categories")
      .withColumnRenamed("Content Rating", "Content_Rating")
      .withColumnRenamed("Last Updated", "Last_Updated")
      .withColumnRenamed("Current Ver", "Current_Version")
      .withColumnRenamed("Android Ver", "Minimum_Android_Version")

    //__________________Questão-> Fazer isto  tudo num df ou repartir em vários dfs__________________

    //Price Dolar/Euro Conv
    val df2priceE = df2tempRename.withColumn("Price", col("Price") * 0.9)

    //X Date conversion (String->DateType)
    //val df3DateConv = df3EuroConv.col("Last_Updated").cast(TimestampType))
    //val df3DateConv = df2priceE.withColumn("Last_Updated", to_timestamp(col("Last_Updated"), "dd/MM/yyyy"))

    // Size (String -> (Clean) -> Double)
    // Define a regular Scala function
    val sizeD: String => String = _.filter(_.isDigit)
    // Define a UDF that wraps the upper Scala function defined above
    // You could also define the function in place, i.e. inside udf
    // but separating Scala functions from Spark SQL's UDFs allows for easier testing
    val sizeDUDF = udf(sizeD)
    val df2Size = df2priceE.withColumn("Size", sizeDUDF(col("Size")))
      .na.replace(Seq("Size"), Map("" -> "null"))

    // Genres (String -> List[String])
    val split: String => Array[String] = _.split(";")
    val splitUDF = udf(split)
    val df2 = df2Size.withColumn("Genres", splitUDF(col("Genres")))

    //__________________Questão-> Fazer isto  tudo num df ou repartir em vários dfs__________________

    //Para cada App juntar todas as categorias
    val df2JoinCats = df2.groupBy("App")
      .agg(collect_list("Categories") as "Categories")
      .orderBy("App")

    df2.createOrReplaceTempView("viewDf2")

    // In case of App duplicates (for all columns except categories), the remaining columns should have the same values as the ones on the row with the maximum number of reviews (compare example 1 with 3).
    val df2bestRevs = spark.sql("SELECT * FROM viewDf2 WHERE (App, Reviews) IN (SELECT App, MAX (Reviews) AS MaxReview FROM viewDf2 GROUP BY App)").drop("Categories")

    val df2final = df2bestRevs.join(df2JoinCats, "App")

    //df2final.show(300)

    //____________________EX 4____________________

    val df3 = df2final.join(df1, "App")

    //Write df3 to parketFile
    df3.coalesce(1).write.mode("overwrite").parquet("results/Ex_4")
    //Find the created file in FS
    val fileParquet1 = fs.globStatus(new Path("results/Ex_4/part*"))(0).getPath().getName()
    //Rename File
    fs.rename(new Path("results/Ex_4/" + fileParquet1), new Path("results/Ex_4/googleplaystore_cleaned.parquet"))

    //____________________EX 5____________________

    df3.createOrReplaceTempView("viewDf3")

    val df4 = spark.sql("SELECT Genres, COUNT(Genres) as COUNT, AVG(Rating) as Average_Rating, AVG(Average_Sentiment_Polarity) as Average_Sentiment_Polarity FROM viewDf3 GROUP BY Genres")
    //df4.show()

    //Write df4 to parketFile
    df4.coalesce(1).write.mode("overwrite").parquet("results/Ex_5")
    //Find the created file in FS
    val fileParquet2 = fs.globStatus(new Path("results/Ex_5/part*"))(0).getPath().getName()
    //Rename File
    fs.rename(new Path("results/Ex_5/" + fileParquet2), new Path("results/Ex_5/googleplaystore_metrics.parquet"))

    df1.show()
    df4.show()
  }

}